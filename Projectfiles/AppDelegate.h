//
//  AppDelegate.h
//  Ghost
//
//  Created by Brian Chu on 10/29/12.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "KKAppDelegate.h"

@interface AppDelegate : KKAppDelegate

extern NSMutableDictionary* user;
extern BOOL noPush;
extern NSMutableArray *words;


- (void)registerForRemoteNotifications;


@end

@compatibility_alias AppController AppDelegate;
