//
//  AppDelegate.m
//  Ghost
//
//  Created by Brian Chu on 10/29/12.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "AppDelegate.h"
#import "InterfaceLayer.h"
#import "GamesTableLayer.h"
#import "GameLayer.h"
#import "ChatLayer.h"


@implementation AppDelegate

NSMutableDictionary* user;
BOOL noPush;
NSMutableArray *words;



-(void) initializationComplete
{
#ifdef KK_ARC_ENABLED
	CCLOG(@"ARC is enabled");
#else
	CCLOG(@"ARC is either not available or not enabled");
#endif
    
	[MGWU loadMGWU:@"tr4xcd3c4d3b4t3ap1hunn1tf0cusprvnt3xt1nct1onthxtakegr88888888c4rst4nfh4rvm1tm1ssionqtczjem1of13rdajqk4ny3191jdafdmfasdf"];
    

    
    
    [MGWU forceFacebook];
    [MGWU useFacebook];
    [MGWU preFacebook];
    [MGWU useNativeFacebookLogin];

	
	
	//To flag whether push notifications are disabled
	noPush = NO; //not disabled
    
    
    
    UIApplication *application = [UIApplication sharedApplication];
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    
}




-(id) alternateView
{
	return nil;
    
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)tokenId {
	[MGWU registerForPush:tokenId];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [MGWU gotPush:userInfo];
    NSLog(@"worked");
	
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
	{
		//Auto refresh views when a message or move has been received (push notification)
		//If move has been received
        CCScene* runningScene = CCDirector.sharedDirector.runningScene;
        if ([[userInfo allKeys] containsObject:@"gameid"])
        {
            
            for (CCNode* child in runningScene.children)
            {
			//if current view is in game, refresh the game
                if ([child isMemberOfClass: [GameLayer class]])
                {
                    GameLayer* gameLayer = (GameLayer*) child;
                    if ([[gameLayer.game objectForKey:@"gameid"] isEqualToNumber:[userInfo objectForKey:@"gameid"]])
                        [gameLayer refresh];
                    break;
                    
                }
                //Else if the current view displayed is the InterfaceLayer, refresh list of games
                else if ([child isMemberOfClass:[InterfaceLayer class]])
                {
                    [(InterfaceLayer*) child refresh];
                    break;
                }
            }
        }
		//If message has been received
        else if ([[userInfo allKeys] containsObject:@"from"])
        {
            for (CCNode* child in runningScene.children)
            {
			//If the current view is in the chat, refresh the chat
                if ([child isMemberOfClass:[ChatLayer class]])
                {
                    ChatLayer *chatTableLayer = (ChatLayer*)child;
                    if ([chatTableLayer.friendID isEqualToString:[userInfo objectForKey:@"from"]])
                        [chatTableLayer refresh];
                }
				else if ([child isMemberOfClass: [GameLayer class]])
                {
                    GameLayer* gameLayer = (GameLayer*) child;
                    if ([gameLayer.opponentName isEqualToString:[userInfo objectForKey:@"from"]])
                        [gameLayer refresh];
                    break;
                    
                }
                else if ([child isMemberOfClass:[InterfaceLayer class]])
                {
                    [(InterfaceLayer*) child refresh];
                    break;
                }
            }
        }
    }
    else
	{
		if ([[userInfo allKeys] containsObject:@"gameid"])
			[MGWU logEvent:@"push_clicked" withParams:@{@"type":@"move"}];
		else if ([[userInfo allKeys] containsObject:@"from"])
			[MGWU logEvent:@"push_clicked" withParams:@{@"type":@"message"}];
	}
}


- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
    [MGWU failedPush:error];
    NSLog(@"fail");
	//push is disabled
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
	[MGWU gotLocalPush:notification];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     [[NSUserDefaults standardUserDefaults] synchronize];
    CCScene* runningScene = CCDirector.sharedDirector.runningScene;
    for (CCNode* child in runningScene.children)
        if ([child isMemberOfClass: [QuizLayer class]])
        {
            //if you're in a guess, treat it as a challenge (to prevent cheating)
        }
	
	[director stopAnimation];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	
	[director startAnimation];
    
    NSString* background = @"FALSE";
    [MGWU setObject:background forKey:@"background"];
    
    //refresh the layer that is running when the app is entered
    CCScene* runningScene = CCDirector.sharedDirector.runningScene;
    for (CCNode* child in runningScene.children)
    {
        if ([child isMemberOfClass:[InterfaceLayer class]])
        {
            [(InterfaceLayer*) child refresh];
            break;
        }

        if ([child isMemberOfClass: [GameLayer class]])
        {
            [(GameLayer*)child refresh];
            break;
            
        }
        if ([child isMemberOfClass:[ChatLayer class]])
        {
            [(ChatLayer*)child refresh];
            break;
        }
    }
}

-(void) applicationWillResignActive:(UIApplication *)application
{
	[director pause];
    NSString* background = @"TRUE";
    [MGWU setObject:background forKey:@"background"];
}

-(void) applicationDidBecomeActive:(UIApplication *)application
{
	[director resume];
    
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [MGWU handleURL:url];
}


@end
