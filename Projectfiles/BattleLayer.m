//
//  BattleLayer.m
//  SatGame
//
//  Created by Keshav Rao on 7/17/13.
//
//

#import "BattleLayer.h"
#import "InterfaceLayer.h"

#import "CCControlExtension.h"
#import "AppDelegate.h"
#import "CCDirector+PopTransition.h"
#import "ChatLayer.h"



@implementation BattleLayer

-(id) init
{
    self = [super init];
    
    
    
    
    NSString *word = [MGWU objectForKey:@"word"];
    
    

    
    
    CCLabelTTF *sent = [CCLabelTTF labelWithString:@"You sent the word: " fontName:@"Helvetica" fontSize:34];
    sent.verticalAlignment = kCCVerticalTextAlignmentTop;
    sent.position = ccp(160, 380);
    [self addChild:sent];
    
    
    CCLabelTTF *question = [CCLabelTTF labelWithString:word fontName:@"Helvetica" fontSize:40];
    question.position = ccp(160, 300);
    [self addChild:question];

    CCControlButton * playButton = [self standardButtonWithTitle:@"Return To Homepage" fontSize:24 selector:@selector(play) preferredSize:CGSizeMake(300, 80)];
    playButton.anchorPoint = ccp(0.5, 1.0);
    playButton.position = ccp(160, 180);
    [self addChild:playButton];
    
    

    
    
 
    return self;
}
-(void) play
{
    CCTransitionFlipX* transition = [CCTransitionFlipX transitionWithDuration:0.5f scene:[InterfaceLayer scene]];
    [CCDirector.sharedDirector pushScene:transition];
    
}

+(id) scene
{
    CCScene *scene = [super scene];
    BattleLayer* layer = [BattleLayer node];
	[scene addChild: layer];
	return scene;
}
@end