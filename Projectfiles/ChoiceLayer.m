//
//  ChoiceLayer.m
//  SatGame
//
//  Created by Keshav Rao on 7/15/13.
//
//

#import "ChoiceLayer.h"
#import "QuizLayer.h"
#import "GameLayer.h"
#import "StartLayer.h"
#import "GamesTableLayer.h"
#import "GameLayer.h"
#import "CCDirector+PopTransition.h"
#import "BattleLayer.h"



@implementation ChoiceLayer



-(id) init
{
    self = [super init];
    
    

    NSArray *allwords = [MGWU objectForKey:@"otherwords"];
    
    
    
    
    if (allwords.count == 0)
    {
        QuizLayer *quizLayer = [[QuizLayer alloc] init];
        
        [quizLayer createQuestion];
        
        allwords = [MGWU objectForKey:@"otherwords"];
        
        
        
    }
    

    
    
    
    
    
//      w = arc4random_uniform([allwords count]);z
    
    w = arc4random() % [allwords count];
    x = arc4random() % [allwords count];

    y = arc4random() % [allwords count];

    z = arc4random() % [allwords count];
    
    while(x == w || x == y || x == z || y == z || w == z || w == y)
    {
        w = arc4random() % [allwords count];
        x = arc4random() % [allwords count];
        
        y = arc4random() % [allwords count];
        
        z = arc4random() % [allwords count];
    }
    
    

//      x = arc4random_uniform([allwords count ]);
//      y = arc4random_uniform([allwords count]);
//      z = arc4random_uniform([allwords count]);
     word1 = [allwords objectAtIndex:w];
     word2 = [allwords objectAtIndex:x];
     word3 = [allwords objectAtIndex:y];
     word4 = [allwords objectAtIndex:z];
    
    CCLabelTTF *wordtitle= [CCLabelTTF labelWithString:@"Select a word to challenge" fontName:@"Nexa Bold" fontSize:24];
    wordtitle.anchorPoint = ccp(0.5, 1.0);
    wordtitle.position = ccp(160, 400);
    [self addChild:wordtitle];
    
    choiceA = [self standardButtonWithTitle:word1 fontSize:12 selector:@selector(choice1) preferredSize:CGSizeMake(200, 80)];
    //choiceA.position = ccp(labelQuestion.position.x, labelQuestion.position.y - labelQuestion.contentSize.height - 8);
    choiceA.anchorPoint =ccp(0.5, 1.0);
    choiceA.position = ccp(100 , 320);
    [self addChild:choiceA];
    
    choiceB = [self standardButtonWithTitle:word2 fontSize:12 selector:@selector(choice2) preferredSize:CGSizeMake(200, 80)];
    //choiceA.position = ccp(labelQuestion.position.x, labelQuestion.position.y - labelQuestion.contentSize.height - 8);
    choiceB.anchorPoint =ccp(0.5, 1.0);
    choiceB.position = ccp(100 , 240);
    [self addChild:choiceB];
    
    choiceC = [self standardButtonWithTitle:word3 fontSize:12 selector:@selector(choice3) preferredSize:CGSizeMake(200, 80)];
    //choiceA.position = ccp(labelQuestion.position.x, labelQuestion.position.y - labelQuestion.contentSize.height - 8);
    choiceC.anchorPoint =ccp(0.5, 1.0);
    choiceC.position = ccp(100 , 160);
    [self addChild:choiceC];
    
    choiceD = [self standardButtonWithTitle:word4 fontSize:12 selector:@selector(choice4) preferredSize:CGSizeMake(200, 80)];
    //choiceA.position = ccp(labelQuestion.position.x, labelQuestion.position.y - labelQuestion.contentSize.height - 8);
    choiceD.anchorPoint =ccp(0.5, 1.0);
    choiceD.position = ccp(100 , 80);
    [self addChild:choiceD];
    
    
    
    return  self;
    
}

- (void)choice1
{
    int a = w;
    [MGWU setObject:word1 forKey:@"word"];
    
    NSNumber *wordchoice = [NSNumber numberWithInt:a];
    
    
    move = @{@"wordchoice":wordchoice};
    
    NSLog(@"%@", wordchoice);
    
    [MGWU setObject:move forKey:@"move"];
    
    GameLayer *gameLayer = [[GameLayer alloc] init];

    
    [gameLayer sendword:wordchoice];
    
    

//    NSMutableDictionary *user = [MGWU objectForKey:@"user"];
//   
//    
//    NSString *opponent = [MGWU objectForKey:@"opponent"];
//    
//    NSString *playerName = [MGWU objectForKey:@"playerName"];
//    
//    
//    
//    
//    NSDictionary *gameData = @{[user objectForKey:@"username"]:@"", opponent:@""};
//    
//    NSString *pushMessage = [NSString stringWithFormat:@"%@ began a game with you", playerName];
    
    //move is a custom dictionary (see above)
    //moveNumber is 0
    //gameID is 0
    //gameState is @"started"
    //gameData is a custom dictionary (see above)
    //opponent is the username of the opponent
    //pushMessage is a custom string (see above)
//    [MGWU move:move withMoveNumber:0 forGame:0 withGameState:@"started" withGameData:gameData againstPlayer:opponent withPushNotificationMessage:pushMessage withCallback:@selector(moveThere) onTarget:self];
//    
//    [MGWU logEvent:@"began_game" withParams:[MGWU objectForKey:@"move"]];
  
     

}
- (void)choice2
{
    int a = x;
    [MGWU setObject:word2 forKey:@"word"];
    
    NSNumber *wordchoice = [NSNumber numberWithInt:a];
    
    move = @{@"wordchoice":wordchoice};
    
    
    [MGWU setObject:move forKey:@"move"];
    
    GameLayer *gameLayer = [[GameLayer alloc] init];
    
    [gameLayer sendword:wordchoice];
    
    
    
}
- (void)choice3
{
    int a = w;
    [MGWU setObject:word3 forKey:@"word"];
    
    NSNumber *wordchoice = [NSNumber numberWithInt:a];
    
    move = @{@"wordchoice":wordchoice};
    
    [MGWU setObject:move forKey:@"move"];
    
    GameLayer *gameLayer = [[GameLayer alloc] init];
    
    [gameLayer sendword:wordchoice];
    
        
}
- (void)choice4
{
    int a = z;
    [MGWU setObject:word4 forKey:@"word"];
    
    NSNumber *wordchoice = [NSNumber numberWithInt:a];
    
    move = @{@"wordchoice":wordchoice};
    
    [MGWU setObject:move forKey:@"move"];
    
    GameLayer *gameLayer = [[GameLayer alloc] init];
    
    NSLog(@"%@", [MGWU objectForKey:@"game"]);
    
    [gameLayer sendword:wordchoice];
    
    
}




+(id) scene
{
    CCScene *scene = [super scene];
    ChoiceLayer* layer = [ChoiceLayer node];
	[scene addChild: layer];
	return scene;
}


- (void)moveThere
{
    
    GameLayer *gameLayer =[[GameLayer alloc] init];
    NSMutableDictionary *newGame;
    [gameLayer moveCompleted:newGame];

//    
//    CCTransitionFlipX* transition = [CCTransitionFlipX transitionWithDuration:0.5f scene:[BattleLayer scene]];
//    [CCDirector.sharedDirector pushScene:transition];

}
//-(CCScene*) sceneWithSelf
//{
//    CCScene *scene = [[self superclass] scene];
//    ChoiceLayer* layer = [ChoiceLayer node];
//	[scene addChild: layer];
//	return scene;
//}
@end
