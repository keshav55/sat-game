//
//  GameData.h
//  SatGame
//
//  Created by Keshav Rao on 8/22/13.
//
//

#import <Foundation/Foundation.h>

//an NSObject is the most basic Objective-C object
//We subclass it since our singleton is nothing more than a wrapper around several properties
@interface GameData : NSObject

//the keyword "nonatomic" is a property declaration
//nonatomic properties have better performance than atomic properties (so use them!)
@property (nonatomic) NSArray* defthings;

@property (nonatomic) NSMutableDictionary *dict;

//Static (class) method:
+(GameData*) sharedData;
@end