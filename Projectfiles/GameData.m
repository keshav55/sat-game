//
//  GameData.m
//  SatGame
//
//  Created by Keshav Rao on 8/22/13.
//
//
#import "GameData.h"

@implementation GameData

@synthesize defthings;
@synthesize dict;

//static variable - this stores our singleton instance
static GameData *sharedData = nil;

+(GameData*) sharedData
{
    
    //If our singleton instance has not been created (first time it is being accessed)
    if(sharedData == nil)
    {
        //create our singleton instance
        sharedData = [[GameData alloc] init];
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *textFilePath = [bundle pathForResource:@"TestingAgain" ofType:@"txt"];
        NSString *fileContents=[NSString stringWithContentsOfFile:textFilePath encoding:NSASCIIStringEncoding error:NULL];
        
        //    NSLog(fileContents);
        
        NSMutableArray *lines = [[NSMutableArray alloc] initWithArray:[fileContents componentsSeparatedByString:@"\n"]];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        
        NSMutableArray *defchoices =  [[NSMutableArray alloc] init];
        
        
        
        int * number = lines.count - 1;
        
        for (int i = 0; i < (int)number; i ++)
        {
            //        NSLog([lines objectAtIndex:i]);
            
            NSString *line = [lines objectAtIndex:i];
            
            
            
            
            
            NSArray *components = [line componentsSeparatedByString:@" - "];
            
            
            
            
            NSString * word = [components objectAtIndex:0];
            
            
            
            
            NSString * def = [components objectAtIndex:1];
            
            
            
            
            
            NSString * otherdef = [components objectAtIndex:1];
            
            
            
            
            [defchoices addObject:otherdef];
            
            
            [dict setObject:def forKey:word];
            
            
        }
        
        sharedData.dict = [[NSMutableDictionary alloc] init];
        sharedData.defthings = [[NSArray alloc] init];
        
        sharedData.dict = dict;
        sharedData.defthings = defchoices;
        
    }
    
    //if the singleton instance is already created, return it
    return sharedData;
}

@end
