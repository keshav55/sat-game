//
//  GameLayer.h
//  Ghost
//
//  Created by Brian Chu on 11/12/12.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "StyledCCLayer.h"
#import "QuizLayer.h"
#import "ChoiceLayer.h"
#import "GuessLayer.h"
#import "BattleLayer.h"


@class CCControlButton, ResultPopup;
@interface GameLayer : StyledCCLayer 
{
    //Dictionary containing game object
	NSMutableDictionary *game;
    NSDictionary *move;
	//Username of opponent
	NSString *opponent;
	//Variable to save whether the game is being created
	BOOL new;
    int moveNumber;


	//Names for displaying / push notifiction messages
    NSString *friendFullName;
	NSString *playerName;
	NSString *opponentName;
	
    //Chat icon in nav bar
	CCMenuItemImage* chatButton;
    CCLabelTTF* chatLabel;
	
	//Game specific UIElements
    CCLabelTTF* word;
	CCLabelTTF* player;
	CCLabelTTF* otherPlayer;
	CCLabelTTF* playerScore;
	CCLabelTTF* opponentScore;
	CCLabelTTF* lastResult;
	CCLabelTTF* lastWord;
	CCLabelTTF* vs;
	
	CCControlButton* play;
	CCControlButton* re;
	CCControlButton* define;
	CCControlButton* moreGames;
    CCControlButton* review;


	CCSprite *divider;
    
    //Variable to control reloading of game
	BOOL inChat;
	BOOL inGuess;
    
    //Challenge outcome + word challenged
	NSString *challengeReason;
	NSString *challengeOutcome;
	NSString *oldWord;
	
	//Images to display profile pictures
	CCSprite* otherGuy;
	CCSprite* me;
    
    //Guess layer and PopupViewController to push onto scene
    ChoiceLayer *choiceLayer;
    __weak ResultPopup* result;
    QuizLayer *quizLayer;

    
}
@property NSMutableDictionary *game;
@property NSString *opponent, *playerName, *opponentName;

@property BOOL inGuess;


//-(CCScene*) sceneWithSelf;
-(void) setupGame;
- (void)loadGame;
//- (void)quit;
- (void)refresh;
- (void)re:(id)sender;
- (void)moveCompleted:(NSMutableDictionary *)newGame;
- (void)guessed:(NSNumber *) question;
-(void) sendword:(NSNumber *) question;
-(void) sendquiz;
-(void) endgame;



-(CCScene*) sceneWithSelf;
@end
