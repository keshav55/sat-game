//
//  GameLayer.m
//  Ghost
//
//  Created by Brian Chu on 11/12/12.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "GameLayer.h"
#import "InterfaceLayer.h"

#import "CCControlExtension.h"
#import "AppDelegate.h"
#import "CCDirector+PopTransition.h"
#import "ChatLayer.h"
#import "DefinitionViewController.h"
#import "Points.h"
#import "WinLayer.h"
#import "RecapLayer.h"

@implementation GameLayer
@synthesize game, opponent, playerName, opponentName, inGuess;




//return a scene with the layer added to it

-(id) init
{
    self = [super init];
    
    inChat = NO;
    inGuess = NO;
    challengeReason = nil;
    challengeOutcome = nil;
    oldWord = nil;
    
    return self;
}

-(void) onEnter
{
    if (inChat)
    {
        //Only reload the game if returning from chat view (since getMyInfo preloads all the games), and set chat label text to nothing since there are no unread chats
        chatLabel.string=@"";
        if ([[game objectForKey:@"turn"] isEqualToString:opponent])
            [MGWU getGame:[[game objectForKey:@"gameid"] intValue] withCallback:@selector(gotGame:) onTarget:self];
        
        inChat = NO;
    }
    if (inGuess)
    {
        inGuess = NO;
        
    }
    
    [super onEnter];
}

-(void) setupGame
{
    //    game = [[NSMutableDictionary alloc] init];
    CGSize screenSize = CCDirector.sharedDirector.winSize;
    
    
    //Top menu bar
    [self addNavBarWithTitle:@"Games"];
    [self addBackButton];
    
    //Chat button
    CCMenuItemImage* chat = [CCMenuItemImage itemWithNormalImage:@"chatButton.png" selectedImage:nil target:self selector:@selector(chat)];
    chat.anchorPoint=ccp(1.0,0.5);
    chat.position = ccp(CCDirector.sharedDirector.winSize.width, 27);
    
    //Chat label (number inside chat icon)
    NSString* chatString;
    if (game && [[game objectForKey:@"newmessages"] intValue] > 0)
        chatString = [NSString stringWithFormat:@"%@", [game objectForKey:@"newmessages"]];
    else
        chatString = @"";
    chatLabel = [CCLabelTTF labelWithString:chatString fontName:@"Nexa Bold" fontSize:12];
    chatLabel.position = ccp(chat.contentSize.width/2.0 + 1, chat.contentSize.height/2.0); //+1 is because chat icon is not symmetric
    [chat addChild:chatLabel];
    [titleBarMenu addChild:chat z:1];
    
    //node to add everything to in order to center everything
    CCNode* centeringNode = [CCNode node];
    centeringNode.position = ccp(screenSize.width/2.0, (screenSize.height - titleBar.contentSize.height)/2.0);
    
    //init images and labels
    me = [CCSprite spriteWithFile:@"profilepicture_bg.png"];
    otherGuy = [CCSprite spriteWithFile:@"profilepicture_bg.png"];
    divider = [CCSprite spriteWithFile:@"Divider.png"];
    word = [CCLabelTTF labelWithString:@" " fontName:@"ghosty" fontSize:40 ];
    player = [CCLabelTTF labelWithString:@" " fontName:@"Roboto-Light" fontSize:18];
    otherPlayer = [CCLabelTTF labelWithString:@" " fontName:@"Roboto-Light" fontSize:18];
    playerScore = [CCLabelTTF labelWithString:@" " fontName:@"Roboto-Light" fontSize:20];
    opponentScore = [CCLabelTTF labelWithString:@" " fontName:@"Roboto-Light" fontSize:20];
    lastResult = [CCLabelTTF labelWithString:@" " fontName:@"Roboto-Light" fontSize:18];
    lastWord = [CCLabelTTF labelWithString:@" " fontName:@"Roboto-Light" fontSize:18];
    vs = [CCLabelTTF labelWithString:@"VS" fontName:@"Roboto-Light" fontSize:40];
    
    
    
    
    
    
    word.horizontalAlignment = kCCTextAlignmentCenter;
    player.horizontalAlignment = kCCTextAlignmentLeft;
    otherPlayer.horizontalAlignment = kCCTextAlignmentRight;
    playerScore.horizontalAlignment = kCCTextAlignmentLeft;
    opponentScore.horizontalAlignment = kCCTextAlignmentRight;
    lastResult.horizontalAlignment = kCCTextAlignmentCenter;
    lastWord.horizontalAlignment = kCCTextAlignmentCenter;
    vs.horizontalAlignment = kCCTextAlignmentCenter;
    
    me.position = ccp(57,186);
    otherGuy.position = ccp(263,186);
    divider.position = ccp(160,314);
    word.position = ccp(160,45);
    player.position = ccp(20, 136);
    player.anchorPoint=ccp(0,0.5);
    otherPlayer.position = ccp(300, 136);
    otherPlayer.anchorPoint=ccp(1,0.5);
    playerScore.position = ccp(58, 242);
    opponentScore.position = ccp(264, 242);
    lastResult.position = ccp(160, 334);
    lastWord.position = ccp(160, 363);
    vs.position = ccp(160, 196);
    
    int screenHeight = CCDirector.sharedDirector.winSize.height;
    screenHeight -= titleBar.contentSize.height;
    
    me.position = ccp(me.position.x, screenHeight - me.position.y);
    otherGuy.position = ccp(otherGuy.position.x, screenHeight - otherGuy.position.y);
    divider.position = ccp(divider.position.x, screenHeight - divider.position.y);
    word.position = ccp(word.position.x, screenHeight - word.position.y);
    player.position = ccp(player.position.x, screenHeight - player.position.y);
    otherPlayer.position = ccp(otherPlayer.position.x, screenHeight - otherPlayer.position.y);
    playerScore.position = ccp(playerScore.position.x, screenHeight - playerScore.position.y);
    opponentScore.position = ccp(opponentScore.position.x, screenHeight - opponentScore.position.y);
    lastResult.position = ccp(lastResult.position.x, screenHeight - lastResult.position.y);
    lastWord.position = ccp(lastWord.position.x, screenHeight - lastWord.position.y);
    vs.position = ccp(vs.position.x, screenHeight - vs.position.y);
    
    [self addChild:me];
    [self addChild:otherGuy];
    [self addChild:divider];
    [self addChild:word];
    [self addChild:player];
    [self addChild:otherPlayer];
    [self addChild:playerScore];
    [self addChild:opponentScore];
    [self addChild:lastResult];
    [self addChild:lastWord];
    [self addChild:vs];
    
    
    //Add buttons:
    play = [self standardButtonWithTitle:@"PLAY" font:@"Roboto-Light" fontSize:20 target:self selector:@selector(play) preferredSize:CGSizeMake(300, 61)];
    re = [self standardButtonWithTitle:@"REFRESH" font:@"Roboto-Light" fontSize:20 target:self selector:@selector(refresh) preferredSize:CGSizeMake(300, 61)];
    moreGames = [self standardButtonWithTitle:@"MORE GAMES" font:@"Roboto-Light" fontSize:20 target:self selector:@selector(moreGames:) preferredSize:CGSizeMake(173, 57)];
    review = [self standardButtonWithTitle:@"REVIEW" font:@"Roboto-Light" fontSize:20 target:self selector:@selector(review) preferredSize:CGSizeMake(173, 57)];
    
    play.position = ccp(160,92);
    re.position = ccp(160,92);
    if (CCDirector.sharedDirector.winSize.height==480)
        //avoid the button being pushed out on smaller screens (non-iPhone 5 screens)
        moreGames.position = ccp(160,392);
    review.position = ccp(160, 342);
    
    play.position = ccp(play.position.x, screenHeight -play.position.y);
    re.position = play.position;
    moreGames.position = ccp(review.position.x, screenHeight - (review.position.y + 60));
    review.position = ccp(review.position.x, screenHeight - review.position.y);
    
    
    [self addChild:play];
    [self addChild:re];
    [self addChild:moreGames];
    [self addChild:review];
    
    
    if (!game) {
        review.visible = NO;
    }
    NSString *gamestate = [game objectForKey:@"gamestate"];
    if ([gamestate isEqualToString:@"started"])
    {
        review.visible = NO;
    }
    
    NSMutableArray *testanswers = [MGWU objectForKey:@"testanswers"];
    
    if (testanswers.count < 5)
    {
        review.visible = NO;
    }
    NSDictionary *gamedata = [game objectForKey:@"gamedata"];
    NSString *victory = [gamedata objectForKey:@"victory"];
    
    
    if (![victory  isEqual: @"not"] && victory)
    {
        CCLabelTTF *lastgame = [CCLabelTTF labelWithString:victory fontName:@"Roboto-Light" fontSize:14];
        
        lastgame.position = ccp(review.position.x - 5, review.position.y + 40);
        
        
        [self addChild:lastgame];
    }
    //Load the game
    [self loadGame];
    otherPlayer.string = opponentName;
    player.string = [InterfaceLayer shortName:[user objectForKey:@"name"]];
    
    
    [MGWU setObject:user forKey:@"user"];
    [MGWU setObject:opponent forKey:@"opponent"];
    [MGWU setObject:playerName forKey:@"playerName"];
    
    //For the Recap Screen - this enables us to view the opponent score
    //    NSDictionary *gamedata = [[MGWU objectForKey:@"game"]objectForKey:@"gamedata"];
    NSMutableArray *opptestanswers = [MGWU objectForKey:@"opptestanswers"];
    if (opptestanswers) {
        [MGWU setObject:opptestanswers forKey:@"opptestanswers"];
    }
    else
        opptestanswers = [gamedata objectForKey:@"testanswers"];
    if (opptestanswers) {
        [MGWU setObject:opptestanswers forKey:@"opptestanswers"];
    }
    
}

//Go to GuessLayer
-(void) play
{
    if (!game) {
        
        
        NSArray *allwords = [MGWU objectForKey:@"otherwords"];
        
        
        [MGWU setObject:@"yes" forKey:@"opptestanswers"];
        
        if (allwords.count == 0)
        {
            quizLayer = [[QuizLayer alloc] init];
            
            [quizLayer createQuestion];
            
            allwords = [MGWU objectForKey:@"otherwords"];
            
            
        }
        
        int v = arc4random() % [allwords count];
        int w = arc4random() % [allwords count];
        int x = arc4random() % [allwords count];
        
        int y = arc4random() % [allwords count];
        
        int z = arc4random() % [allwords count];
        
        while(v == w || v == x || v == y || v == z || w == x || w == y || w == z || x == y || x == z || y == z)
        {
            v = arc4random() % [allwords count];
            w = arc4random() % [allwords count];
            x = arc4random() % [allwords count];
            
            y = arc4random() % [allwords count];
            
            z = arc4random() % [allwords count];
        }
        NSNumber *q = [NSNumber numberWithInt:v];
        NSNumber *q2 = [NSNumber numberWithInt:w];
        NSNumber *q3 = [NSNumber numberWithInt:x];
        NSNumber *q4 = [NSNumber numberWithInt:y];
        NSNumber *q5 = [NSNumber numberWithInt:z];
        
        
        [MGWU setObject:q forKey:@"q"];
        [MGWU setObject:q2 forKey:@"q2"];
        [MGWU setObject:q3 forKey:@"q3"];
        [MGWU setObject:q4 forKey:@"q4"];
        [MGWU setObject:q5 forKey:@"q5"];
        
        NSString *wordchoice = @"Continue";
        
        move = @{@"wordchoice":wordchoice};
        
        [MGWU setObject:@"10" forKey:@"oppquizscore"];
        
        NSLog(@"%@", wordchoice);
        
        [MGWU setObject:move forKey:@"move"];
        [[NSUserDefaults standardUserDefaults] setObject:move forKey:@"move"];
        
        
        
        NSNumber* movecount =  [game objectForKey:@"movecount"];
        movecount = @([movecount intValue] + 1);
        [MGWU setObject:movecount forKey:@"movecount"];
        
        CCTransitionFlipX* transition = [CCTransitionFlipX transitionWithDuration:0.5f scene:[QuizLayer scene]];
        [CCDirector.sharedDirector pushScene:transition];
        
    }
    if (game){
        
        NSDictionary *gamedata = [game objectForKey:@"gamedata"];
        NSString *thegame = [[gamedata objectForKey:@"move"]objectForKey:@"wordchoice"];
        
        
        if ([thegame isEqualToString:@"Continue"]) {
            [MGWU setObject:game forKey:@"game"];
            NSMutableArray *opptestanswers = [gamedata objectForKey:@"testanswers"];
            
            
            
            NSNumber* movecount =  [game objectForKey:@"movecount"];
            movecount = @([movecount intValue] + 1);
            [MGWU setObject:movecount forKey:@"movecount"];
            
            NSNumber *q = [gamedata objectForKey:@"q"];
            NSNumber *q2 = [gamedata objectForKey:@"q2"];
            NSNumber *q3 = [gamedata objectForKey:@"q3"];
            NSNumber *q4 = [gamedata objectForKey:@"q4"];
            NSNumber *q5 = [gamedata objectForKey:@"q5"];
            NSNumber *oppquizscore = [gamedata objectForKey:@"p1quizscore"];
            
            [MGWU setObject:q forKey:@"q"];
            [MGWU setObject:q2 forKey:@"q2"];
            [MGWU setObject:q3 forKey:@"q3"];
            [MGWU setObject:q4 forKey:@"q4"];
            [MGWU setObject:q5 forKey:@"q5"];
            
            NSString *wordchoice = @"New";
            
            move = @{@"wordchoice":wordchoice};
            [MGWU setObject:move forKey:@"move"];
            
            [MGWU setObject:opptestanswers forKey:@"opptestanswers"];
            [MGWU setObject:oppquizscore forKey:@"oppquizscore"];
            
            
        }
        else if ([thegame isEqualToString:@"New"]) {
            
            
            NSArray *allwords = [MGWU objectForKey:@"otherwords"];
            
            
            [MGWU setObject:@"yes" forKey:@"opptestanswers"];
            
            if (allwords.count == 0)
            {
                quizLayer = [[QuizLayer alloc] init];
                
                [quizLayer createQuestion];
                
                allwords = [MGWU objectForKey:@"otherwords"];
                
                
            }
            
            int v = arc4random() % [allwords count];
            int w = arc4random() % [allwords count];
            int x = arc4random() % [allwords count];
            
            int y = arc4random() % [allwords count];
            
            int z = arc4random() % [allwords count];
            
            while(v == w || v == x || v == y || v == z || w == x || w == y || w == z || x == y || x == z || y == z)
            {
                v = arc4random() % [allwords count];
                w = arc4random() % [allwords count];
                x = arc4random() % [allwords count];
                
                y = arc4random() % [allwords count];
                
                z = arc4random() % [allwords count];
            }
            NSNumber *q = [NSNumber numberWithInt:v];
            NSNumber *q2 = [NSNumber numberWithInt:w];
            NSNumber *q3 = [NSNumber numberWithInt:x];
            NSNumber *q4 = [NSNumber numberWithInt:y];
            NSNumber *q5 = [NSNumber numberWithInt:z];
            
            
            [MGWU setObject:q forKey:@"q"];
            [MGWU setObject:q2 forKey:@"q2"];
            [MGWU setObject:q3 forKey:@"q3"];
            [MGWU setObject:q4 forKey:@"q4"];
            [MGWU setObject:q5 forKey:@"q5"];
            
            NSString *wordchoice = @"Continue";
            
            move = @{@"wordchoice":wordchoice};
            
            [MGWU setObject:move forKey:@"move"];
            
            NSLog(@"%@", wordchoice);
            
            [MGWU setObject:@"10" forKey:@"oppquizscore"];
            
            NSNumber* movecount =  [game objectForKey:@"movecount"];
            movecount = @([movecount intValue] + 1);
            [MGWU setObject:movecount forKey:@"movecount"];
            
            [MGWU setObject:game forKey:@"game"];
            
            
            CCTransitionFlipX* transition = [CCTransitionFlipX transitionWithDuration:0.5f scene:[QuizLayer scene]];
            [CCDirector.sharedDirector pushScene:transition];
            
            
        }
        
        
        
        
        
        
        //        NSNumber *yes = [[game objectForKey:@"moves"] valueForKey:@"wordchoice"];
        ////        NSLog(@"%@", yes);
        //        [MGWU setObject:yes forKey:@"this"];
        
        
        
        
        
        CCTransitionFlipX* transition = [CCTransitionFlipX transitionWithDuration:0.5f scene:[QuizLayer scene]];
        [CCDirector.sharedDirector pushScene:transition];
        
    }
    
    
}

//called when back arrow is pressed in nav bar
-(void) back
{
    //pop scene, slide in new scene from the left
    [CCDirector.sharedDirector popSceneWithTransition:[CCTransitionSlideInL class] duration:0.25f];
    
}

//Go to chat layer
-(void) chat
{
    ChatLayer* chatLayer = [[ChatLayer alloc] initWithFriendID:opponent];
    inChat = YES;
    
    //slide in from the right
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[chatLayer sceneWithSelf]];
    [CCDirector.sharedDirector pushScene:transition];
}

//Go to definition view (DefinitionViewController) - UIKit


- (void)moreGames:(id)sender
{
    [MGWU displayCrossPromo];
    
    
    
}

- (void)gotGame:(NSMutableDictionary*)g
{
    
    
    //Update game object and reload game
    NSString *gameID = [NSString stringWithFormat:@"%@",[game objectForKey:@"gameid"]];
    //Prevent cheating by not reloading game if you challenged but haven't started the next round (since the server doesn't know about the challenge yet)
    NSMutableDictionary *savedGame = [NSMutableDictionary dictionaryWithDictionary:[MGWU objectForKey:gameID]];
    
    
    
    if ([savedGame isEqualToDictionary:@{}])
        
        
    {
        game = g;
        NSLog(@"%@", game);
        //If you're friends with the player, add friendName to the game dictionary
        if (friendFullName)
            [game setObject:friendFullName forKey:@"friendName"];
    }
    else
    {
        [game setObject:[g objectForKey:@"newmessages"] forKey:@"newmessages"];
    }
    
    //Update view
    [self loadGame];
}

- (void) loadGame
{
    //If game doesn't exist yet
    if (!game)
    {
        //Flag it as a new game, and set game specific variables to reflect a new game
        new = YES;
        word.string = @"";
        playerScore.string = @"";
        opponentScore.string = @"";
        [play setTitle:@"BEGIN GAME" forState:CCControlStateNormal];
        play.visible=YES;
        re.visible=NO;
        moreGames.visible=NO;
        divider.visible = NO;
        title.string = @"START";
        title.fontSize = 25;
    }
    //If game already exists
    else
    {
        //Flag it as a game in progress
        new = NO;
        
        
        
        NSString* gameState = [game objectForKey:@"gamestate"];
        NSString* turn = [game objectForKey:@"turn"];
        
        //Set opponent name from list of players
        NSArray* players = [game objectForKey:@"players"];
        if ([[players objectAtIndex:0] isEqualToString:[user objectForKey:@"fbid"]])
            opponent = [players objectAtIndex:1];
        else
            opponent = [players objectAtIndex:0];
        
        if ([[game allKeys] containsObject:@"friendName"])
        {
            friendFullName = [game objectForKey:@"friendName"];
            opponentName = [InterfaceLayer shortName:friendFullName];
            playerName = [InterfaceLayer shortName:[user objectForKey:@"name"]];
        }
        else
        {
            opponentName = [opponent stringByReplacingOccurrencesOfString:@"_" withString:@"."];
            playerName = [[user objectForKey:@"username"] stringByReplacingOccurrencesOfString:@"_" withString:@"."];
        }
        
        //Set board to reflect gameData
        NSDictionary *gameData = [game objectForKey:@"gamedata"];
        NSLog(@"%@", gameData);
        
        NSNumber *score = [gameData objectForKey:[user objectForKey:@"username"]];
        [MGWU setObject:score forKey:@"score"];
        
        NSNumber *oppscore = [gameData objectForKey:opponent];
        
        playerScore.string = [NSString stringWithFormat:@"%i",score.intValue];
        opponentScore.string = [NSString stringWithFormat:@"%i",oppscore.intValue];
        
        [MGWU setObject:score forKey:@"userscore"];
        [MGWU setObject:oppscore forKey:@"oppscore"];
        
        //
        
        
        
        
        //
        //		playerScore.string = [[[gameData objectForKey:[user objectForKey:@"username"]] uppercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
        //		while ([playerScore.string length] < 5)
        //		{
        //			playerScore.string = [playerScore.string stringByAppendingString:@">"];
        //		}
        //		opponentScore.string = [[[gameData objectForKey:opponent] uppercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
        //		while ([opponentScore.string length] < 5)
        //		{
        //			opponentScore.string = [opponentScore.string stringByAppendingString:@">"];
        //		}
        
        
        //manually setting dimensions of string prevents string from spilling off screen and fixes a bug where letters like "j", "g", and "q" are cut off at the bottom when using a custom font
        
        
        if ([[gameData objectForKey:@"lastdefinition"] isEqualToString:@""])
        {
            divider.visible=NO;
            
        }
        else
        {
            divider.visible=YES;
        }
        
        moreGames.visible = YES;
        
        //Based on state of game, set game specific variables
        if ([gameState isEqualToString:@"ended"])
        {
            if ([[gameData allKeys] containsObject:@"winner"])
            {
                if ([[gameData objectForKey:@"winner"] isEqualToString:opponent])
                    word.string = [NSString stringWithFormat:@"%@ beat you", opponentName];
                else
                    word.string = [NSString stringWithFormat:@"You beat %@", opponentName];
                [self rescaleWord];
            }
            else
                word.string = [NSString stringWithFormat:@"Completed Game Against %@", opponentName];
            
            play.visible=NO;
            [re setTitle:@"REMATCH" forState:CCControlStateNormal];
            re.visible = YES;
            self.title.string = @"GAME OVER";
        }
        else if ([turn isEqualToString:opponent])
        {
            play.visible=NO;
            [re setTitle:@"REFRESH" forState:CCControlStateNormal];
            re.visible=YES;
            self.title.string = @"WAITING...";
        }
        else if (![turn isEqualToString:opponent])
            
        {
            
            [play setTitle:@"PLAY" forState:CCControlStateNormal];
            play.visible=YES;
            re.visible=NO;
            self.title.fontSize = 15;
            self.title.string = @"YOUR TURN";
            
        }
    }
    
    //Based on how many new messages, set chat button title
    if (game && [[game objectForKey:@"newmessages"] intValue] > 0)
        chatLabel.string = [NSString stringWithFormat:@"%@", [game objectForKey:@"newmessages"]];
    else
        chatLabel.string = @"";
    
}

//make sure word doesn't spill off screen
-(void) rescaleWord
{
    if (word.contentSize.width > CCDirector.sharedDirector.winSize.width - 20) //20 = padding
        word.scale = (CCDirector.sharedDirector.winSize.width - 20) / word.contentSize.width;
}

//If player exits the app in the middle of guessing, treat it as a challenge
//- (void)quit
//{
//	[quizLayer challenge:nil];
//}//
-(void) sendquiz
{
    moveNumber = [[MGWU objectForKey:@"movecount"] intValue];
    
    
    
    if (moveNumber == 1)
    {
        
        
        
        
        //Create the game on the server
        move = [MGWU objectForKey:@"move"];
        
        
        
        
        
        
        user = [MGWU objectForKey:@"user"];
        opponent = [MGWU objectForKey:@"opponent"];
        
        
        
        
        
        NSDictionary *gameData = @{@"q":[MGWU objectForKey:@"q"],@"q2":[MGWU objectForKey:@"q2"], @"q3":[MGWU objectForKey:@"q3"], @"q4":[MGWU objectForKey:@"q4"], @"q5":[MGWU objectForKey:@"q5"], [user objectForKey:@"username"]:@"0", opponent:@"0", @"testanswers":[MGWU objectForKey:@"testanswers"],@"p1quizscore":[MGWU objectForKey:@"userquizscore"],@"move":move};
        
        playerName = [MGWU objectForKey:@"playerName"];
        
        NSString *pushMessage = [NSString stringWithFormat:@"%@ began a game with you", playerName];
        
        //move is a custom dictionary (see above)
        //moveNumber is 0
        //gameID is 0
        //gameState is @"started"
        //gameData is a custom dictionary (see above)
        //opponent is the username of the opponent
        //pushMessage is a custom string (see above)
        
        
        
        [MGWU move:move withMoveNumber:moveNumber forGame:0 withGameState:@"started" withGameData:gameData againstPlayer:opponent withPushNotificationMessage:pushMessage withCallback:@selector(moveCompleted:) onTarget:self];
        
        
        
        
        [MGWU logEvent:@"began_game"];
        
        
        
        
        
        
    }
    
    else
    {
        game = [MGWU objectForKey:@"game"];
        //Create the game on the server
        move = [MGWU objectForKey:@"move"];
        
        user = [MGWU objectForKey:@"user"];
        
        opponent = [MGWU objectForKey:@"opponent"];
        
        NSDictionary *oldgamedata = [game objectForKey:@"gamedata"];
        NSLog(@"%@", oldgamedata);
        
        
        [MGWU setObject:[MGWU objectForKey:@"opptestanswers"] forKey:@"opptestanswers"];
        
        
        
        
        //    NSNumber *score = [MGWU objectForKey:@"newscore"];
        
        
        NSString *victory = [MGWU objectForKey:@"victory"];
        
        if (!victory) {
            victory = [NSString stringWithFormat:@"not"];
        }
        //so it would be score and oppscore okay just keep it at 0 for testing purposes
        
        
        NSDictionary *gameData = @{@"q":[MGWU objectForKey:@"q"],@"q2":[MGWU objectForKey:@"q2"], @"q3":[MGWU objectForKey:@"q3"], @"q4":[MGWU objectForKey:@"q4"], @"q5":[MGWU objectForKey:@"q5"],[user objectForKey:@"username"]:[MGWU objectForKey:@"userscore"], @"p1quizscore":[MGWU objectForKey:@"userquizscore"],opponent:[MGWU objectForKey:@"oppscore"],@"move":move,@"testanswers":[MGWU objectForKey:@"testanswers"],@"victory":victory};
        
        
        
        
        playerName = [MGWU objectForKey:@"playerName"];
        opponent = [MGWU objectForKey:@"opponent"];
        move = [MGWU objectForKey:@"move"];
        
        NSString *pushMessage = [NSString stringWithFormat:@"%@ sent a move", playerName];
        
        //move is a custom dictionary (see above)
        //moveNumber is 0
        //gameID is 0
        //gameState is @"started"
        //gameData is a custom dictionary (see above)
        //opponent is the username of the opponent
        //pushMessage is a custom string (see above)
        
        int gameID = [[game objectForKey:@"gameid"] intValue];
        
        NSLog(@"%d", moveNumber);
        
        
        [MGWU move:move withMoveNumber:moveNumber forGame:gameID withGameState:@"inprogress" withGameData:gameData againstPlayer:opponent withPushNotificationMessage:pushMessage withCallback:@selector(moveCompleted:) onTarget:self];
        
        
        [MGWU logEvent:@"question_selected"];
        
    }
    
    
    
}
-(void) sendword:(NSNumber *) question

{
    moveNumber = [[MGWU objectForKey:@"movecount"] intValue];
    NSNumber *score = [MGWU objectForKey:@"newscore"];
    
    
    if (moveNumber == 1)
    {
        
        
        
        
        //Create the game on the server
        move = [MGWU objectForKey:@"move"];
        move = @{@"question":question};
        
        
        
        
        user = [MGWU objectForKey:@"user"];
        opponent = [MGWU objectForKey:@"opponent"];
        
        
        
        NSDictionary *gameData = @{@"q":question,[user objectForKey:@"username"]:@"0", opponent:@"0"};
        
        
        
        NSString *pushMessage = [NSString stringWithFormat:@"%@ began a game with you", playerName];
        
        //move is a custom dictionary (see above)
        //moveNumber is 0
        //gameID is 0
        //gameState is @"started"
        //gameData is a custom dictionary (see above)
        //opponent is the username of the opponent
        //pushMessage is a custom string (see above)
        
        
        
        [MGWU move:move withMoveNumber:moveNumber forGame:0 withGameState:@"started" withGameData:gameData againstPlayer:opponent withPushNotificationMessage:pushMessage withCallback:@selector(moveCompleted:) onTarget:self];
        
        
        
        
        [MGWU logEvent:@"began_game"];
        
        
        
        
        
        
    }
    else
    {
        game = [MGWU objectForKey:@"game"];
        //Create the game on the server
        move = [MGWU objectForKey:@"move"];
        move = @{@"question":question};
        
        user = [MGWU objectForKey:@"user"];
        
        opponent = [MGWU objectForKey:@"opponent"];
        
        NSDictionary *oldgamedata = [game objectForKey:@"gamedata"];
        NSLog(@"%@", oldgamedata);
        
        NSNumber *oppscore = [oldgamedata objectForKey:opponent];
        
        
        
        
        
        
        //    NSNumber *score = [MGWU objectForKey:@"newscore"];
        
        NSLog(@"%@", score);
        
        
        NSDictionary *gameData = @{@"q":question,[user objectForKey:@"username"]:score, opponent:oppscore};
        
        
        
        
        NSString *pushMessage = [NSString stringWithFormat:@"%@ sent a move", playerName];
        
        //move is a custom dictionary (see above)
        //moveNumber is 0
        //gameID is 0
        //gameState is @"started"
        //gameData is a custom dictionary (see above)
        //opponent is the username of the opponent
        //pushMessage is a custom string (see above)
        
        int gameID = [[game objectForKey:@"gameid"] intValue];
        
        NSLog(@"%d", moveNumber);
        
        
        [MGWU move:move withMoveNumber:moveNumber forGame:gameID withGameState:@"inprogress" withGameData:gameData againstPlayer:opponent withPushNotificationMessage:pushMessage withCallback:@selector(moveCompleted:) onTarget:self];
        
        
        [MGWU logEvent:@"question_selected"];
    }
    
}

-(void) endgame
{
    moveNumber = [[MGWU objectForKey:@"movecount"] intValue];
    
    
    NSNumber *score = [MGWU objectForKey:@"newscore"];
    
    NSString *pushMessage;
    
    game = [MGWU objectForKey:@"game"];
    //Create the game on the server
    move = [MGWU objectForKey:@"move"];
    
    user = [MGWU objectForKey:@"user"];
    
    opponent = [MGWU objectForKey:@"opponent"];
    
    NSDictionary *oldgamedata = [game objectForKey:@"gamedata"];
    NSLog(@"%@", oldgamedata);
    
    NSNumber *oppscore = [oldgamedata objectForKey:opponent];
    
    
    //    NSNumber *score = [MGWU objectForKey:@"newscore"];
    
    NSLog(@"%@", score);
    
    
    NSDictionary *gameData = @{[user objectForKey:@"username"]:score, opponent:oppscore};
    
    if ([[gameData objectForKey:@"winner"] isEqualToString:opponent])
    {
        pushMessage = [NSString stringWithFormat:@"%@ beat you! :(", playerName];
    }
    else
    {
        pushMessage = [NSString stringWithFormat:@"You beat %@! :)", playerName];
    }
    
    //move is a custom dictionary (see above)
    //moveNumber is incremented from the old move number
    //gameID is the same as the old gameID
    //gameState is @"ended"
    //gameData is a custom dictionary (see above)
    //opponent is the username of the opponent
    //pushMessage is a custom string (see above)
    int gameID = [[game objectForKey:@"gameid"] intValue];
    [MGWU move:move withMoveNumber:moveNumber forGame:gameID withGameState:@"ended" withGameData:gameData againstPlayer:opponent withPushNotificationMessage:pushMessage withCallback:@selector(moveCompleted:) onTarget:self];
    
}

- (void)guessed:(NSNumber *) question
{
    //If the game is just starting
    
    
    
    NSLog(@"%@", game);
    
    if (!game)
    {
        
        
        
        
        //Create the game on the server
        //		move = [MGWU objectForKey:@"move"];
        //
        //
        //
        //        user = [MGWU objectForKey:@"user"];
        //        opponent = [MGWU objectForKey:@"opponent"];
        
        
        
        NSDictionary *gameData = @{@"q":question,[user objectForKey:@"username"]:@"", opponent:@""};
        
        
        
        NSString *pushMessage = [NSString stringWithFormat:@"%@ began a game with you", playerName];
        
        //move is a custom dictionary (see above)
        //moveNumber is 0
        //gameID is 0
        //gameState is @"started"
        //gameData is a custom dictionary (see above)
        //opponent is the username of the opponent
        //pushMessage is a custom string (see above)
        
        
        
        [MGWU move:move withMoveNumber:0 forGame:0 withGameState:@"started" withGameData:gameData againstPlayer:opponent withPushNotificationMessage:pushMessage withCallback:@selector(moveCompleted:) onTarget:self];
        
        
        
        
        [MGWU logEvent:@"began_game" withParams:@{@"question":question}];
        
    }
    else
    {
        NSDictionary *oldGameData = [game objectForKey:@"gamedata"];
        int gameID = [[game objectForKey:@"gameid"] intValue];
        
        
        
        
        
        
        
        BOOL ended = FALSE;
        NSDictionary *gameData;
        
        NSString *gameEnded;
        
        
        //If the game was ended
        if (ended)
        {
            //Send the move up to the server and end the game
            move = @{@"question":question};
            
            int moveNumber = [[game objectForKey:@"movecount"] intValue] + 1;
            
            NSString *gameState = @"ended";
            
            NSString *pushMessage;
            
            if ([[gameData objectForKey:@"winner"] isEqualToString:opponent])
            {
                pushMessage = [NSString stringWithFormat:@"%@ beat you! :(", playerName];
            }
            else
            {
                pushMessage = [NSString stringWithFormat:@"You beat %@! :)", playerName];
            }
            
            //move is a custom dictionary (see above)
            //moveNumber is incremented from the old move number
            //gameID is the same as the old gameID
            //gameState is @"ended"
            //gameData is a custom dictionary (see above)
            //opponent is the username of the opponent
            //pushMessage is a custom string (see above)
            [MGWU move:move withMoveNumber:moveNumber forGame:gameID withGameState:gameState withGameData:gameData againstPlayer:opponent withPushNotificationMessage:pushMessage withCallback:@selector(moveCompleted:) onTarget:self];
            
            
            gameEnded = @"yes";
        }
        //Otherwise, update the gameData and save it to prevent cheating, but don't send anything up to the server since you need to begin the next round
        else
        {
            [game setObject:gameData forKey:@"gamedata"];
            NSString *gameID = [NSString stringWithFormat:@"%@",[game objectForKey:@"gameid"]];
            [MGWU setObject:game forKey:gameID];
            [self loadGame];
            gameEnded = @"no";
        }
        
        //open graph magic
        if ([challengeOutcome isEqualToString:@"won"])
        {
            //open graph magic
            //Publish open graph
            //Publish leaderboard
            NSString *opUsername = [opponent stringByReplacingOccurrencesOfString:@"_" withString:@"."];
            NSString *u = [NSString stringWithFormat: @"https://graph.facebook.com/%@/picture", opUsername];
            NSDictionary *params;
            if ([[game allKeys] containsObject:@"friendName"])
                params = @{@"object":@"profile", @"title":[game objectForKey:@"friendName"], @"username":opUsername, @"image":u};
            else
                params = @{@"object":@"profile", @"title":opUsername, @"username":opUsername, @"image":u};
            [MGWU publishOpenGraphAction:@"beat" withParams:params];
            
            int roundswon = [[[NSUserDefaults standardUserDefaults] objectForKey:@"totalroundswon"] intValue];
            roundswon += 1;
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:roundswon] forKey:@"totalroundswon"];
            [MGWU publishOpenGraphAction:@"highscore" withParams:@{@"score":[NSNumber numberWithInt:roundswon]}];
        }
        
        
        
        
        
        
        //If not a challenge
        else
        {
            challengeOutcome = nil;
            challengeReason = nil;
            
            NSString *oldq = [oldGameData objectForKey:@"q"];
            
            //Send move up to the server
            
            if ([oldq isEqualToString:@""])
            {
                move = @{@"challenge":[oldGameData objectForKey:@"lastword"], @"question":question};
                [MGWU logEvent:@"began_round" withParams:@{@"question":question}];
            }
            else
                move = @{@"question":question};
            
            int moveNumber = [[game objectForKey:@"movecount"] intValue] + 1;
            
            NSString *gameState = @"inprogress";
            
            NSNumber *w;
            
            if ([oldq isEqualToString:@""])
                w = question;
            else
                w = [NSString stringWithFormat:@"%@ %@", oldWord, question];
            
            NSMutableDictionary *gameData = [NSMutableDictionary dictionaryWithDictionary:oldGameData];
            [gameData setObject:w forKey:@"word"];
            
            NSString *pushMessage;
            
            pushMessage = [NSString stringWithFormat:@"%@ played you, go play now!", playerName];
            
            //move is a custom dictionary (see above)
            //moveNumber is incremented from the old move number
            //gameID is the same as the old gameID
            //gameState is @"inprogress"
            //gameData is a custom dictionary (see above)
            //opponent is the username of the opponent
            //pushMessage is a custom string (see above)
            [MGWU move:move withMoveNumber:moveNumber forGame:gameID withGameState:gameState withGameData:gameData againstPlayer:opponent withPushNotificationMessage:pushMessage withCallback:@selector(moveCompleted:) onTarget:self];
        }
    }
    
    
    //Show the challenge popup
    [self displayChallengePopup];
    
    
    if (question)
        [MGWU logEvent:@"question_selected" withParams:@{@"question":question}];
}




- (void)displayChallengePopup
{
    //If there was a challenge (so challenge reason won't be nil), display a popup showing the result of the challenge
    if (challengeReason)
    {
        NSString* titleString, *messageString;
        result = [ResultPopup node];
        if ([challengeOutcome isEqualToString:@"won"])
            titleString = @"You Won the Challenge!";
        else
            titleString = @"You Lost the Challenge";
        if ([challengeReason isEqualToString:@"not_word"])
            messageString = [NSString stringWithFormat:@"%@ is not a word, nor a word fragment", oldWord];
        else if ([challengeReason isEqualToString:@"complete_word"])
            messageString = [NSString stringWithFormat:@"%@ is a complete word", oldWord];
        else
            messageString = [NSString stringWithFormat:@"%@ is not a word, but is a word fragment", oldWord];
        //       //[rescaleTitleWithString:titleString];
        //[popup rescaleMessageWithString:messageString];
        ////        popup.delegate=self;
        ////        [self addChild:popup z:4];
    }
}

//PopupDelegate method - Dismisses the challenge outcome popup
-(void)dismiss
{
    [self removeChild:result cleanup:YES];
}



//Increment the score string that is passed in by adding a letter
- (NSString*)incrementScore:(NSString*)oldScore
{
    oldScore = [oldScore stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([oldScore isEqualToString:@""])
        return @"g";
    else if ([oldScore isEqualToString:@"g"])
        return @"gh";
    else if ([oldScore isEqualToString:@"gh"])
        return @"gho";
    else if ([oldScore isEqualToString:@"gho"])
        return @"ghos";
    else if ([oldScore isEqualToString:@"ghos"])
        return @"ghost";
    else
        return @"";
}

- (void)refresh
{
    //If game object exists, reload the game
    if (game)
    {
        [MGWU getGame:[[game objectForKey:@"gameid"] intValue] withCallback:@selector(gotGame:) onTarget:self];
        
        
    }
}

//Action to make a move, refresh the game, or start a rematch
- (void)re:(id)sender
{
    
    //If it is not your turn, refresh the game
    if ([re.titleLabel.string isEqualToString:@"REFRESH"])
    {
        [self refresh];
        return;
    }
    //If the game is over, start a new game
    else if ([re.titleLabel.string isEqualToString:@"REMATCH"])
    {
        
        game = nil;
        
        [self loadGame];
        return;
    }
}

- (void)moveCompleted:(NSMutableDictionary*)newGame
{
    //Refresh the game once a move has been made
    
    //
    game = newGame;
    //
    NSMutableDictionary *oldgame = [MGWU objectForKey:@"game"];
    
    //Remove the opponent from the list of players in case you just started a new game
    [InterfaceLayer.sharedInstance.players removeObject:opponent];
    
    //Remove game from cache
    NSString *gameID = [NSString stringWithFormat:@"%@",[game objectForKey:@"gameid"]];
    [MGWU removeObjectForKey:gameID];
    
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[InterfaceLayer scene]];
    [CCDirector.sharedDirector replaceScene:transition];
    
    
    
    
}
-(void) review
{
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[RecapLayer scene]];
    [CCDirector.sharedDirector replaceScene:transition];
}
-(CCScene*) sceneWithSelf
{
    CCScene *scene = [[self superclass] scene];
    [scene addChild: self];
    return scene;
}

@end