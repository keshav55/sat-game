//
//  GameMechanics.h
//  _MGWU-SideScroller-Template_
//
//  Created by Benjamin Encz on 5/17/13.
//  Modified by Dion Larson.
//

#import <Foundation/Foundation.h>

#import "QuizLayer.h"


typedef NS_ENUM(NSInteger, GameState) {
    GameStatePaused,
    GameStateMenu,
    GameStateRunning
};

/**
 This class stores several global game parameters. It stores 
 references to several entities and sets up e.g. the 'floorHeight'.
 
 This class is used by all entities in the game to access shared ressources.
**/

@interface GameMechanics : NSObject

// determines the current state the game is in. Either menu mode (scene displayed beyond main menu) or gameplay mode.
@property (nonatomic, assign) GameState gameState;


// reference to the main gameplay layer
@property (nonatomic, weak) QuizLayer *gameScene;



// amount of coins on hand for player
@property (nonatomic, assign) int coinsOnHand;

// current reward for level
@property (nonatomic, assign) int currentCoinReward;

// gives access to the shared instance of this class
+ (id)sharedGameMechanics;

// resets the complete State of the sharedGameMechanics. Should be called whenever a new level is started.
- (void)resetGame;

@end
