//
//  GameMechanics.m
//  _MGWU-Level-Template_
//
//  Created by Benjamin Encz on 5/17/13.
//  Modified by Dion Larson on 6/4/13.
//

#import "GameMechanics.h"

@implementation GameMechanics

+ (id)sharedGameMechanics
{
    static dispatch_once_t once;
    static id sharedInstance;
    // Uses GCD (Grand Central Dispatch) to restrict this piece of code to only be executed once
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    
    

    return self;
}

- (void)resetGame
{
    self.gameScene = nil;
    self.gameState = GameStatePaused;
}

- (void)setCoinsOnHand:(int)coinsOnHand
{
    // saves and encryptes the number of coins a player has
    [MGWU setObject:[NSNumber numberWithInt:coinsOnHand] forKey:@"coins"];
}

- (int)coinsOnHand
{
    // retreives the number of coins a player has from encrypted NSUserDefaults
    NSNumber *coins = [MGWU objectForKey:@"coins"];
    return [coins integerValue];
}

@end
