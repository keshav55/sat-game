//
//  GuessLayer.h
//  Ghost
//
//  Created by Brian Chu on 1/8/13.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "StyledCCLayer.h"

@protocol GuessDelegate <NSObject>

-(void)guessed:(NSString*)letter;

@end

@interface GuessLayer : StyledCCLayer <UITextFieldDelegate>
{
	CCLabelTTF* time;
	CCLabelTTF* word;
	CCControlButton* challenge;
	UITextField *letter;
	id <GuessDelegate> delegate;
	NSString *w;
	
	NSTimer *timer;
	int counter; 
}

@property id<GuessDelegate> delegate;
@property NSString *w;

-(id) initWithWord: (NSString*) wordString;
- (void)challenge:(id)sender;
-(CCScene*) sceneWithSelf;
@end
