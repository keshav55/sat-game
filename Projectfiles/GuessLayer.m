//
//  GuessLayer.m
//  Ghost
//
//  Created by Brian Chu on 1/8/13.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "GuessLayer.h"
#import "CCControlButton.h"

@implementation GuessLayer

@synthesize delegate, w;

-(id) initWithWord: (NSString*) wordString
{
    self = [super init];
    
    w = wordString;
    
    time = [CCLabelTTF labelWithString:@"30" fontName:@"Nexa Bold" fontSize:40]; //countdown timer
    word = [CCLabelTTF labelWithString:@"c a l l" fontName:@"ghosty" fontSize:40]; //word (so far)
    //challenge button
    challenge = [self standardButtonWithTitle:@"CHALLENGE" font:@"Nexa Bold" fontSize:40 target:self selector:@selector(challenge:) preferredSize:CGSizeMake(280, 66)];
    
    time.position = ccp(160,43);
    word.position = ccp(160,118);
    challenge.position = ccp(160,204);
    
    int screenHeight = CCDirector.sharedDirector.winSize.height;
    
    time.position = ccp(time.position.x, screenHeight - time.position.y);
	word.position = ccp(word.position.x, screenHeight - word.position.y);
	challenge.position = ccp(challenge.position.x, screenHeight - challenge.position.y);
    
    [self addChild:time];
    [self addChild:word];
    [self addChild:challenge];
    
    //Hidden UITextField - used to input single letter
    //UIKit:
    //Note: Position of UIKit objects is relative to upper left corner
    //CGRectMake(x,y,width,height)
    letter= [[UITextField alloc] initWithFrame:CGRectMake(145,430, 30, 30)];

    letter.delegate = self;
    letter.placeholder = @"";
    letter.font = [UIFont fontWithName:@"Helvetica" size:14];
	[letter setReturnKeyType:UIReturnKeyDefault];
	[letter setAutocorrectionType:UITextAutocorrectionTypeNo];
	[letter setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [letter setKeyboardAppearance:UIKeyboardAppearanceAlert];
    

	if ([w length] > 0)
		word.string = [[w stringByAppendingString:@" >"] uppercaseString];
	else
		word.string = @">";;
	
    //make sure word doesn't spill off screen
    if (word.contentSize.width > CCDirector.sharedDirector.winSize.width - 20) //20 = padding
        word.scale = (CCDirector.sharedDirector.winSize.width - 20) / word.contentSize.width;
    
    //can't challenge for one letter
	if ([w length] < 2)
		challenge.visible=NO;
	
	if ([w isEqualToString:@""])
	{
		time.string = @"Select a letter to begin the round";
        time.fontSize=18;
	}
	else
	{
        //turn on countdown timer
		timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countdown) userInfo:nil repeats:YES];
		counter = 30;
	}
    return self;
}

//called every timer interval
- (void)countdown
{
    //lose if timer goes to 0
	if (counter == 0)
	{
        [letter resignFirstResponder];
		[delegate guessed:nil];
		[timer invalidate];
	}
	else
	{
		counter--;
		time.string = [NSString stringWithFormat:@"%d", counter];
	}
	
}

//challenge button
- (void)challenge:(id)sender
{
    [letter resignFirstResponder]; //hide keyboard
	[delegate guessed:nil];
	[timer invalidate];
}

//UITextFieldDelegate - called when text field changes
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	NSString *potentialLetter = string;
	
    NSCharacterSet *nonLetters = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz"] invertedSet];
	
    if ([potentialLetter stringByTrimmingCharactersInSet:nonLetters].length != potentialLetter.length) {
        return NO;
    } else {
        [letter resignFirstResponder]; //hide keyboard
		[delegate guessed:potentialLetter];
		[timer invalidate];
        return YES;
    }
}

//make sure the letter text field is removed when the layer is removed
-(void) onExitTransitionDidStart
{
    [letter removeFromSuperview];
    [super onExitTransitionDidStart];
}

//make sure to reattach the letter text field when the layer is added again
-(void) onEnter
{
    [[[CCDirector sharedDirector] view] addSubview:letter];
    [letter becomeFirstResponder];
    [super onEnter];
}

//return scene with layer added to it
-(CCScene*) sceneWithSelf
{
    CCScene *scene = [[self superclass] scene];
	[scene addChild: self];
	return scene;
}



@end
