//
//  InterfaceLayer.m
//  Ghost
//
//  Created by Brian Chu on 10/29/12.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "InterfaceLayer.h"
#import "CCControlExtension.h"
#import "StartLayer.h"
#import "GamesTableLayer.h"
#import "GameLayer.h"
#import "BattleLayer.h"
#import "PlayersTableLayer.h"
#import "InviteTableLayer.h"
#import "BadgedCCMenuItemSprite.h"
#import "AppDelegate.h"
#import "CCMenuNoSwallow.h"


@interface InterfaceLayer ()
@end

@implementation InterfaceLayer
@synthesize currentTableLayer, games,gamesYourTurn, gamesTheirTurn, gamesCompleted;
@synthesize players, nonPlayers, recommendedFriends, newFriends, gamesTab, playersTab, inviteTab;

static InterfaceLayer* sharedInstance; //allows access to interface layer from any other layer

+(id) scene
{
    CCScene *scene = [super scene];
    
    //Add GamesTableLayer
    GamesTableLayer* gamesTableLayer = [[GamesTableLayer alloc] init];
    [scene addChild: gamesTableLayer z:0];
    
    //overlay InterfaceLayer on top of the GamesTableLayer
    //GamesTableLayer is then switched out depending on the table we want
    InterfaceLayer* interfaceLayer = [[InterfaceLayer alloc] initWithTableLayer:gamesTableLayer];
    [scene addChild: interfaceLayer z:1];
    return scene;
}

+(InterfaceLayer*) sharedInstance
{
    return sharedInstance;
}

-(id) initWithTableLayer: (TableLayer*) firstTableLayer
{
    if ((self = [super init]))
    {
        
        
        CCDirector* director = CCDirector.sharedDirector;
        sharedInstance = self;
        
        //Top menu bar (methods are implemented in StyledCCLayer)
        [self addNavBarWithTitle:@"GAMES"];
        [self addBackButton];
        
        CCMenuItemImage* refresh = [CCMenuItemImage itemWithNormalImage:@"RefreshButton.png" selectedImage:nil disabledImage:nil target:self selector:@selector(refresh)];
        refresh.anchorPoint=ccp(1.0,0.5);
        refresh.position = ccp(director.winSize.width - 5, titleBar.position.y);
        [titleBarMenu addChild:refresh];
        
        //properly initialize tableLayer
        gamesTableLayer=(GamesTableLayer*) firstTableLayer;
        
        [self setupTableLayer:firstTableLayer];
    }
    return self;
}

//sets up the table layer we're loading in next
-(void) setupTableLayer: (TableLayer*) newTableLayer;
{
    currentTableLayer=newTableLayer;
    [newTableLayer setupWithTabBarHeight:0 titleBarHeight:titleBar.contentSize.height];
}

//called when back arrow is pressed in nav bar
-(void) back
{
    if (currentTableLayer == playersTableLayer) {
        [self gamesClick];
    } else if (currentTableLayer == inviteTableLayer) {
        [self playersClick];
    } else {
        //slide in scene from the left
        CCTransitionSlideInR* transition = [CCTransitionSlideInL transitionWithDuration:0.25f scene:[StartLayer scene]];
        [CCDirector.sharedDirector replaceScene:transition];
    }
}

-(void) refresh
{
    [MGWU getMyInfoWithCallback:@selector(loadedUserInfo:) onTarget:self];
}

//Take user to update screen if he taps download update (see first bit of code in loadedUserInfo method)
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == updateAlertView)
    {
        if (updateAlertView.numberOfButtons == 1 || buttonIndex == 1)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:updateURL]];
        else
            [updateAlertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        updateAlertView = nil;
    }
}

- (void)loadedUserInfo:(NSMutableDictionary*)userInfo
{
   
  
    //THIS CODE IS COMMENTED OUT FOR THE TEMPLATE
    //You likely want to use this when your app is ready for the app store
    //Checks whether the server has any info about a new version, prompts user to update, or forces user to update depending on response from server
    //	if ([[userInfo allKeys] containsObject:@"appversion"])
    //	{
    //		updateURL = [userInfo objectForKey:@"updateurl"];
    //		NSString *latestVersion = [userInfo objectForKey:@"appversion"];
    //		NSString *curVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    //		if ([curVersion compare:latestVersion options:NSNumericSearch] == NSOrderedAscending)
    //		{
    //			if (!updateAlertView)
    //			{
    //				if ([[userInfo allKeys] containsObject:@"forceupdate"])
    //					updateAlertView = [[UIAlertView alloc] initWithTitle:@"Update Required" message:@"You must download the latest update to continue playing" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Download Now!", nil];
    //				else
    //					updateAlertView = [[UIAlertView alloc] initWithTitle:@"Update Available" message:@"A new update has been released on the app store" delegate:self cancelButtonTitle:@"No Thanks" otherButtonTitles:@"Download Now!", nil];
    //				[updateAlertView show];
    //			}
    //		}
    //	}

    
    
    user = [userInfo objectForKey:@"info"];
    games = [userInfo objectForKey:@"games"];
   
    players = [userInfo objectForKey:@"friends"];
    
    
    //	nonPlayers = [MGWU friendsToInvite];
    
    NSArray *playingFriends = [NSArray arrayWithArray:players];
    
    if (currentTableLayer!=playersTableLayer)
    {
        //Set badge number for new friends when they join the game
        int numOldFriends = [[NSUserDefaults standardUserDefaults] integerForKey:@"numFriends"];
        int numNewFriends = [players count];
        if (numNewFriends > numOldFriends)
        {
            newFriends = numNewFriends-numOldFriends;
        }
    }
    
    //Some open graph magic
    if (counter%3 == 0)
    {
        NSMutableArray *followedFriends = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"og_followed"]];
        NSMutableArray *ogPlayingFriends = [[NSMutableArray alloc] init];
        for (NSMutableDictionary *p in players)
        {
            [ogPlayingFriends addObject:[p objectForKey:@"username"]];
        }
        NSPredicate *relativeComplementPredicate = [NSPredicate predicateWithFormat:@"NOT SELF IN %@", followedFriends];
        NSArray *relativeComplement = [ogPlayingFriends filteredArrayUsingPredicate:relativeComplementPredicate];
        if ([relativeComplement count] > 0)
        {
            int r = arc4random()%[relativeComplement count];
            NSString *usernameToFollow = [relativeComplement objectAtIndex:r];
            [followedFriends addObject:usernameToFollow];
            [[NSUserDefaults standardUserDefaults] setObject:followedFriends forKey:@"og_followed"];
            //Publish Open Graph
            //			NSString *opid = [MGWU fbidFromUsername:usernameToFollow];
            //			[MGWU publishOpenGraphAction:@"follow" withParams:@{@"profile":opid}];
        }
    }
    counter++;
    
    
    //Sort games by dateplayed
    NSArray *sortedGames = [games sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = [a objectForKey:@"dateplayed"];
        NSNumber *second = [b objectForKey:@"dateplayed"];
        return [second compare:first];
    }];
    
    games = [NSMutableArray arrayWithArray:sortedGames];
    
    
    //Split up games based on whose turn it is / whether the game is over
    gamesCompleted = [[NSMutableArray alloc] init];
    gamesYourTurn = [[NSMutableArray alloc] init];
    gamesTheirTurn = [[NSMutableArray alloc] init];
    
    NSString *username = [user objectForKey:@"fbid"];
    
    for (NSMutableDictionary *game in games)
    {
        NSString* gameState = [game objectForKey:@"gamestate"];
        NSString* turn = [game objectForKey:@"turn"];
        
        NSString* oppName;
        NSArray* gamers = [game objectForKey:@"players"];
        if ([[gamers objectAtIndex:0] isEqualToString:username])
            oppName = [gamers objectAtIndex:1];
        else
            oppName = [gamers objectAtIndex:0];
        
        if ([gameState isEqualToString:@"ended"])
        {
            [gamesCompleted addObject:game];
            for (NSMutableDictionary *friend in players)
            {
                //Add friendName to game if you're friends
                if ([[friend objectForKey:@"username"] isEqualToString:oppName])
                {
                    [game setObject:[friend objectForKey:@"name"] forKey:@"friendName"];
                    break;
                }
            }
        }
        else if ([turn isEqualToString:[user objectForKey:@"username"]])
        {
            //Preventing cheating
            NSString *gameID = [NSString stringWithFormat:@"%@",[game objectForKey:@"gameid"]];
            NSMutableDictionary *savedGame = [NSMutableDictionary dictionaryWithDictionary:[MGWU objectForKey:gameID]];
            if ([savedGame isEqualToDictionary:@{}])
                savedGame = game;
            else
                [savedGame setObject:[game objectForKey:@"newmessages"] forKey:@"newmessages"];
            [gamesYourTurn addObject:savedGame];
            for (NSMutableDictionary *friend in playingFriends)
            {
                //Add friendName to game if you're friends, remove the friend from list of players (so you can't start a new game with someone you're already playing)
                if ([[friend objectForKey:@"username"] isEqualToString:oppName])
                {
                    [savedGame setObject:[friend objectForKey:@"name"] forKey:@"friendName"];
                    [players removeObject:friend];
                    break;
                }
            }
        }
        else
        {
            [gamesTheirTurn addObject:game];
            for (NSMutableDictionary *friend in playingFriends)
            {
                //Add friendName to game if you're friends, remove the friend from list of players (so you can't start a new game with someone you're already playing)
                if ([[friend objectForKey:@"username"] isEqualToString:oppName])
                {
                    [game setObject:[friend objectForKey:@"name"] forKey:@"friendName"];
                    [players removeObject:friend];
                    break;
                }
            }
        }
    }
    
    //Adding recommended friends:
    recommendedFriends = [[NSMutableArray alloc] init];
    
    NSMutableArray *randomPlayingFriends = [NSMutableArray arrayWithArray:players];
    //	NSMutableArray *randomNonPlayingFriends = [NSMutableArray arrayWithArray:[MGWU friendsToInvite]];
    
    //Shuffle list of friends who play the game
    if ([randomPlayingFriends count] > 0)
    {
        for (NSUInteger i = [randomPlayingFriends count] - 1; i >= 1; i--)
        {
            u_int32_t j = arc4random_uniform(i + 1);
            
            [randomPlayingFriends exchangeObjectAtIndex:j withObjectAtIndex:i];
        }
    }
    
    //Shuffle list of friends who don't play yet
    //	if ([randomNonPlayingFriends count] > 0)
    //	{
    //		for (NSUInteger i = [randomNonPlayingFriends count] - 1; i >= 1; i--)
    //		{
    //			u_int32_t j = arc4random_uniform(i + 1);
    //
    //			[randomNonPlayingFriends exchangeObjectAtIndex:j withObjectAtIndex:i];
    //		}
    //	}
    
    NSUInteger i=0;
    
    for (i = 0; i < 2 && i < [randomPlayingFriends count]; i++)
    {
        [recommendedFriends addObject:[randomPlayingFriends objectAtIndex:i]];
    }
    //	for (int j = i; j < 3 && (j-i < [randomNonPlayingFriends count]); j++)
    //	{
    //		[recommendedFriends addObject:[randomNonPlayingFriends objectAtIndex:j-i]];
    //	}
    //
    //Set badges on tab bar based on games that are your turn and new friends who are playing
    //handle tab button badges
    
    gamesTab.badgeString = [NSString stringWithFormat:@"%d", [gamesYourTurn count]];
    if (newFriends == 0)
        playersTab.badgeString = nil;
    else
        playersTab.badgeString = [NSString stringWithFormat:@"%d", newFriends];
    
    [self refreshAll];
    
}

//refreshes all tables - InviteTableLayer is left out for performance reasons
-(void) refreshAll
{
    [gamesTableLayer reloadData];
    [playersTableLayer reloadData];
}

//Remove table layer
-(void) tableCleanup;
{
    [currentTableLayer removeFromParentAndCleanup:NO];
    [currentTableLayer pauseSchedulerAndActions];
    currentTableLayer=nil;
    disabledItem.isEnabled = YES; //"unselect" the tab button that is pressed down
}

//Games tab button clicked (first tab)
-(void) gamesClick
{
    [self tableCleanup];
    title.fontSize = 15;
    title.string = @"GAMES"; //nav bar title
    
    //if we already have the layer set up (and cached)
    if (gamesTableLayer)
        [gamesTableLayer resumeSchedulerAndActions];
    //otherwise, set up a new layer
    else
    {
        gamesTableLayer=[[GamesTableLayer alloc] init];
        [self setupTableLayer:gamesTableLayer];
    }
    currentTableLayer=gamesTableLayer;
    [CCDirector.sharedDirector.runningScene addChild:currentTableLayer z:0];
    
    //permanently select the tab button to indicate that we are on that tab.
    disabledItem = gamesTab;
    disabledItem.isEnabled=NO;
    //    [currentTableLayer refresh];
}

//Players tab button clicked (second tab)
-(void) playersClick
{
    [self tableCleanup];
    title.fontSize = 15;
    title.string = @"NEW GAME";
    if (playersTableLayer)
    {
        [playersTableLayer resumeSchedulerAndActions];
    }
    else
    {
        playersTableLayer=[[PlayersTableLayer alloc] init];
        [self setupTableLayer:playersTableLayer];
    }
    currentTableLayer=playersTableLayer;
    
    [CCDirector.sharedDirector.runningScene addChild:currentTableLayer z:0];
    
    disabledItem = playersTab;
    disabledItem.isEnabled=NO;
}

// Play Now button pressed
- (void)playNowPressed {
    [MGWU logEvent:@"pressed_play_now_button" withParams:nil];
    
    if ([gamesYourTurn count] > 0)
    {
        /*
         1) If you have open games on which it is your turn to play, play one of these games
         */
        GameLayer *gameLayer = [[GameLayer alloc] init];
        gameLayer.game = [gamesYourTurn objectAtIndex:0];
        [gameLayer setupGame];
        
        //slide in scene from the right
        CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[gameLayer sceneWithSelf]];
        [CCDirector.sharedDirector pushScene:transition];
    } else if ([players count] > 0)
    {
        /*
         2) If you have friends playing NTJ you are currently not having a match with, challenge them
         */
        
        int randPlayer = arc4random()%[players count];
        GameLayer* gameLayer = [[GameLayer alloc] init];
        gameLayer.opponent = [[players objectAtIndex:randPlayer] objectForKey:@"username"];
        gameLayer.opponentName = [InterfaceLayer shortName:[[players objectAtIndex:randPlayer] objectForKey:@"name"]];
        gameLayer.playerName = [InterfaceLayer shortName:[user objectForKey:@"name"]];
        [gameLayer setupGame];
        
        //slide in scene from the right
        CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[gameLayer sceneWithSelf]];
        [CCDirector.sharedDirector pushScene:transition];
    } else
    {
        /*
         3) Start a random match
         */
        [MGWU getRandomGameWithCallback:@selector(gotGame:) onTarget:self];
        
        
    }
}


-(void)gotGame:(NSMutableDictionary*)g
{
    //If error occurs, do nothing
    if (!g)
        return;
    
    //If the server responds with no existing random game, start a new one
    if ([[g objectForKey:@"gameid"] intValue] == 0)
    {
        //Start game with mgwu-random
        GameLayer* gameLayer = [[GameLayer alloc] init];
        gameLayer.opponent = @"mgwu-random";
        gameLayer.opponentName = @"mgwu-random";
        gameLayer.playerName = [user objectForKey:@"username"];
        [gameLayer setupGame];
        
        //slide in scene from the right
        CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[gameLayer sceneWithSelf]];
        [CCDirector.sharedDirector pushScene:transition];
    }
    //Otherwise, join the game that was returned
    else
    {
        //Play game retreived from server
        GameLayer* gameLayer = [[GameLayer alloc] init];
        gameLayer.game = g;
        [gameLayer setupGame];
        
        //slide in scene from the right
        CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[gameLayer sceneWithSelf]];
        [CCDirector.sharedDirector pushScene:transition];
    }
}



//Invite tab button clicked (third tab)
-(void) inviteClick
{
    [self tableCleanup];
    title.fontSize = 15;
    title.string = @"INVITE";
    if (inviteTableLayer)
    {
        [inviteTableLayer resumeSchedulerAndActions];
    }
    else
    {
        inviteTableLayer=[[InviteTableLayer alloc] init];
        [self setupTableLayer:inviteTableLayer];
    }
    currentTableLayer=inviteTableLayer;
    [CCDirector.sharedDirector.runningScene addChild:currentTableLayer z:0];
    disabledItem = inviteTab;
    disabledItem.isEnabled=NO;
}

//abbreviates friend names
+ (NSString*)shortName:(NSString*)friendname
{
    NSArray *names = [friendname componentsSeparatedByString:@" "];
    NSString * firstLetter = [[names objectAtIndex:([names count]-1)] substringToIndex:1];
    NSString *shortname;
    if ([names count] > 1)
        shortname = [[names objectAtIndex:0] stringByAppendingFormat:@" %@", firstLetter];
    else
        shortname = [names objectAtIndex:0];
    shortname = [shortname stringByAppendingString:@"."];
    return shortname;
}

//Every time the game returns to any of the 3 tables (from a game), all three get refreshed
-(void) onEnter
{
    [currentTableLayer refresh];
    [super onEnter];
}

@end