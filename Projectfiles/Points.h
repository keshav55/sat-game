//
//  ScoreboardEntryNode.h
//  _MGWU-Level-Template_
//
//  Created by Benjamin Encz on 5/16/13.
//  Modified by Dion Larson.
//

#import "CCNode.h"

/**
 Displays an icon and a score.
 **/

@interface Points : CCNode

- (void)setTextColor:(ccColor3B)color;
- (void)setTextSize:(int)size;

@property (nonatomic, assign) int score;
@property (nonatomic, strong) NSString *scoreStringFormat;

@end
