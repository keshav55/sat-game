//
//  ScoreboardEntryNode.m
//  _MGWU-Level-Template_
//
//  Created by Benjamin Encz on 5/16/13.
//  Modified by Dion Larson on 6/4/13
//

#import "Points.h"
#import "STYLES.h"

@implementation Points

{
    CCLabelTTF *scoreLabel;
    CCSprite *scoreIcon;
}

@synthesize score = _score;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.scoreStringFormat = @"%d";
        scoreLabel = [CCLabelTTF labelWithString:@"" fontName:@"Times New Roman" fontSize:32];
        [scoreLabel setColor:DEFAULT_FONT_COLOR];
        [self addChild:scoreLabel];
        
        
    }
    
    return self;
}

- (void)setTextColor:(ccColor3B)color
{
    scoreLabel.color = color;
}

- (void)setTextSize:(int)size
{
    scoreLabel.fontSize = size;
    [self updatePositions];
}

- (void)setScore:(int)score
{
    _score = score;
    
    scoreLabel.string = [NSString stringWithFormat:_scoreStringFormat, score];
    [self updatePositions];
}

- (void)updatePositions
{
    scoreIcon.position = ccp(-scoreLabel.contentSize.width/2, 0);
    scoreLabel.position = ccp(scoreIcon.contentSize.width/2, 0);
    self.contentSize = CGSizeMake(scoreIcon.contentSize.width + scoreLabel.contentSize.width, scoreLabel.contentSize.height);
}

@end
