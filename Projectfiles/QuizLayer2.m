//
//  QuizLayer2.m
//  SatGame
//
//  Created by Keshav Rao on 8/24/13.
//
//

#import "QuizLayer2.h"
#import "InterfaceLayer.h"
#import "CCControlButton.h"
#import "TutorialLayer.h"
#import "Timer.h"
#import "WinLayer.h"
#import "GameMechanics.h"
#import "DefinitionViewController.h"
#import "GameLayer.h"
#import "QuizLayer3.h"


@interface QuizLayer2 ()
@end

@implementation QuizLayer2






-(void) createQuestion
{
    
    
    
    data = [GameData sharedData];
    dict = data.dict;
    
    
    
    
    NSArray *keys = [dict allKeys];
    
    
    [MGWU setObject:keys forKey:@"otherwords"];
    
    
    {
        
        //        NSArray *array = [MGWU objectForKey:@"this"];
        //        NSNumber *ok = [array objectAtIndex:0];
        //        int x = [ok intValue];
        //        NSLog(@"%@", ok);
        
        
        
        int x = [[MGWU objectForKey:@"q2"]intValue];
        
        
        
        
        //     NSLog(@"%d", k);
        word = [keys objectAtIndex:x];
        
        
        
        def = [dict objectForKey:word];
        
          [MGWU setObject:def forKey:@"def2"];
        
        
        
        
    }
    
    
    
    
    
    
    
    labelQuestion = [CCLabelTTF labelWithString:word fontName:@"Roboto-Light" fontSize:20];
    labelQuestion.position = ccp(160, size.height-60);
    [self addChild:labelQuestion];
    
    
    
    
    
    
    
}



-(void) createChoices
{
    
    int numIterations = 0;
    
    NSMutableArray *threeDefs =  [[NSMutableArray alloc] init];
    
    if (!data)
    {
        data = [GameData sharedData];
        defchoices = [[NSMutableArray alloc] initWithArray:data.defthings];
    }
    else{
        defchoices = [[NSMutableArray alloc] initWithArray:data.defthings];
    }
    
    while (numIterations < 3)
    {
        
        [defchoices removeObjectIdenticalTo:def];
        
        for (int i = defchoices.count - 1; i >= 0; --i)
        {
            int r = arc4random_uniform(defchoices.count);
            [defchoices exchangeObjectAtIndex:i withObjectAtIndex:r];
            
            
            
            
            NSString *line = [defchoices objectAtIndex:i];
            
            
            
            
            [threeDefs addObject:line];
            
            [defchoices removeLastObject];
            
            
            numIterations++;
            
            if (numIterations == 3)
                break;
        }
    }
    NSMutableArray *randomdefs = threeDefs;
    
    defchoices = [[NSMutableArray alloc] initWithArray:data.defthings];
    
    
    
    choice1 = [randomdefs objectAtIndex:0];
    choice2 = [randomdefs objectAtIndex:1];
    choice3 = [randomdefs objectAtIndex:2];
    
    
    
    
}




-(void) randomPoint
{
    
    
    int yChoose = (arc4random()%(4-1+1))+1;
    
    if (yChoose == 1) {
        y1 = size.height - 100;
        y2 = size.height - 200;
        y3 = size.height - 300;
        y4 = size.height - 400;
        
    }
    
    if (yChoose == 2) {
        y1 = size.height - 200;
        y2 = size.height - 300;
        y3 = size.height - 400;
        y4 = size.height - 100;
    }
    
    if (yChoose == 3) {
        y1 = size.height - 300;
        y2 = size.height - 400;
        y3 = size.height - 100;
        y4 = size.height - 200;
    }
    
    if (yChoose == 4) {
        y1 = size.height - 400;
        y2 = size.height - 100;
        y3 = size.height - 200;
        y4 = size.height - 300;
    }
    
    
}
-(id) init
{
    self = [super init];
    
    
    size = CCDirector.sharedDirector.winSize;
    
    
    
    
    
    
    
    //Ghost logo
    //CCLabelTTF* question = [CCLabelTTF labelWithString:@"question" fontName:@"Times New Roman" fontSize:20];
    
    //labelQuestion.anchorPoint = ccp(0.5, 1.0);
    //labelQuestion.position = ccp(160, size.height-60);
    
    
    
    [self createQuestion];
    
    [self createChoices];
    
    [self randomPoint];
    
    
    
    
    //Play
    
    //        NSUInteger maxval = 100;
    //        if (def.length > maxval) {
    //        [NSString stringWithFormat:@"%@ , def];
    ////            answer.scale = 240 / answer.contentSize.width;
    ////        answer.position = ccp(160, 1100);
    ////        answer.string = def;
    //        }
    
    
    [self formatAnswer];
    [self formatChoice1];
    [self formatChoice2];
    [self formatChoice3];
    
    
    choiceA = [self standardButtonWithTitle:resultDef fontSize:12 selector:@selector(correctAnswer) preferredSize:CGSizeMake(200, 80)];
    choiceA.anchorPoint =ccp(0.5, 1.0);
    //choiceA.position = ccp(labelQuestion.position.x, labelQuestion.position.y - labelQuestion.contentSize.height - 8);
    choiceA.position = ccp(160 , y1);
    [self addChild:choiceA];
    
    
    
    choiceB = [self standardButtonWithTitle:resultChoice1 fontSize:12 selector:@selector(wrongAnswer) preferredSize:CGSizeMake(200, 80)];
    choiceB.anchorPoint = choiceA.anchorPoint;
    choiceB.position =  ccp(choiceA.position.x, y2);
    [self addChild:choiceB];
    
    choiceC = [self standardButtonWithTitle:resultChoice2 fontSize:12 selector:@selector(wrongAnswer) preferredSize:CGSizeMake(200, 80)];
    choiceC.anchorPoint = choiceB.anchorPoint;
    choiceC.position =  ccp(choiceB.position.x, y3);
    [self addChild:choiceC];
    
    
    choiceD = [self standardButtonWithTitle:resultChoice3 fontSize:12 selector:@selector(wrongAnswer) preferredSize:CGSizeMake(200, 80)];
    choiceD.anchorPoint = choiceC.anchorPoint;
    choiceD.position =  ccp(choiceC.position.x, y4);
    [self addChild:choiceD];
    
    //Timer
    size = [[CCDirector sharedDirector] winSize];
    
    //set time to zero
    myTime = totalTime = 10;
    
    timeLabel = [CCLabelTTF labelWithString:@"30" fontName:@"Arial" fontSize:24];
    timeLabel.position = CGPointMake(size.width / 2, size.height - 20);
    // Adjust the label's anchorPoint's y position to make it align with the top.
    timeLabel.anchorPoint = CGPointMake(0.5f, 1.0f);
    // Add the time label
    [self addChild:timeLabel];
    
    
    //update
    
    [self schedule:@selector(update:)];
    
    
   
    
    return self;
}





-(void)howToPlay
{
    
    //slide in scene from the right
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[ResultPopup scene]];
    [CCDirector.sharedDirector replaceScene:transition];
}
-(void)update:(ccTime)dt{
    
    totalTime -= dt;
    currentTime = (int)totalTime;
    if (myTime >= currentTime)
    {
        myTime = currentTime;
        [timeLabel setString:[NSString stringWithFormat:@"%i", myTime]];
        
    }
    
    
    
    if (currentTime == 0)
    {
        //          CCLabelTTF  * gameOver = [CCLabelTTF labelWithString:@"GAMEOVER" fontName:@"Times New Roman" fontSize:20];
        //            gameOver.anchorPoint = ccp(0.5, 1.0);
        //            gameOver.position = ccp(160, 40);
        //            [self addChild:gameOver];
        [self unschedule:_cmd];
        
        choiceA.visible = NO;
        choiceB.visible = NO;
        choiceC.visible = NO;
        choiceD.visible = NO;
        [self wrongAnswer];
        
        //Popup
//        NSString* titleString, *messageString;
//        titleString = @"Time's Up!";
//        messageString = [NSString stringWithFormat:@"GameOver! You lose 1 health point"];
//        wResult = [WrongResultPopup node];
//        [wResult rescaleTitleWithString:titleString];
//        [wResult rescaleMessageWithString:messageString];
//        wResult.delegate=self;
//        [self addChild:wResult
//                     z:4];
        
        
    }
    
}




- (void)correctAnswer
{
	
    choiceA.visible = NO;
    choiceB.visible = NO;
    choiceC.visible = NO;
    choiceD.visible = NO;
    timeLabel.visible   = NO;
    
    
    
    NSString *score = [MGWU objectForKey:@"newscore"];
    int this = [score intValue];
    this = this + 1;
    score = [NSString stringWithFormat:@"%i", this];
    [MGWU setObject:score forKey:@"newscore"];
    
    
    NSMutableArray *testanswers = [MGWU objectForKey:@"testanswers"];
    BOOL answer = YES;
    [testanswers addObject:[NSNumber numberWithBool:answer]];
    [MGWU setObject:testanswers forKey:@"testanswers"];
    
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[QuizLayer3 scene]];
    [CCDirector.sharedDirector replaceScene:transition];
}


- (void)wrongAnswer
{
	
    
    choiceA.visible = NO;
    choiceB.visible = NO;
    choiceC.visible = NO;
    choiceD.visible = NO;
    timeLabel.visible   = NO;
    
    
    NSMutableArray *testanswers = [MGWU objectForKey:@"testanswers"];
    BOOL answer = NO;
    [testanswers addObject:[NSNumber numberWithBool:answer]];
    [MGWU setObject:testanswers forKey:@"testanswers"];
    
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[QuizLayer3 scene]];
    [CCDirector.sharedDirector replaceScene:transition];
}


-(void)dismiss
{
    
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[QuizLayer3 scene]];
    [CCDirector.sharedDirector replaceScene:transition];
    if (end == 10)
    {
        
        GameLayer *gameLayer = [[GameLayer alloc] init];
        [gameLayer endgame];
    }
    
}
-(void)leave
{
    
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[QuizLayer3 scene]];
    [CCDirector.sharedDirector replaceScene:transition];
    
}

-(void)formatAnswer
{
    
    resultDef = [NSMutableString stringWithString:def];
    int lastSpace=0;
    int lastbreak=0;
    NSUInteger i = [resultDef length];
    while(i > 0) {
        if ([resultDef characterAtIndex:i-1]==' ')
            lastSpace=i;
        lastbreak++;
        if (lastbreak>=25) {
            if (lastSpace!=0) {
                [resultDef insertString:@"\n" atIndex: lastSpace];
            } else {
                // we have not found a space in 10 chars, so break where there is no space.
                // no H&J engine here, so we can add the - or not.
                [resultDef insertString:@"-\n" atIndex: i];
                i++;  // since were adding a character, dont skip a character.
            }
            lastbreak=0;
            lastSpace=0;
        }
        i--;
    }
}
-(void)formatChoice1
{
    
    resultChoice1 = [NSMutableString stringWithString:choice1];
    int lastSpace=0;
    int lastbreak=0;
    NSUInteger i = [resultChoice1 length];
    while(i > 0) {
        if ([resultChoice1 characterAtIndex:i-1]==' ')
            lastSpace=i;
        lastbreak++;
        if (lastbreak>=25) {
            if (lastSpace!=0) {
                [resultChoice1 insertString:@"\n" atIndex: lastSpace];
            } else {
                // we have not found a space in 10 chars, so break where there is no space.
                // no H&J engine here, so we can add the - or not.
                [resultChoice1 insertString:@"-\n" atIndex: i];
                i++;  // since were adding a character, dont skip a character.
            }
            lastbreak=0;
            lastSpace=0;
        }
        i--;
    }
}
-(void)formatChoice2
{
    resultChoice2 = [NSMutableString stringWithString:choice2];
    int lastSpace=0;
    int lastbreak=0;
    NSUInteger i = [resultChoice2 length];
    while(i > 0) {
        if ([resultChoice2 characterAtIndex:i-1]==' ')
            lastSpace=i;
        lastbreak++;
        if (lastbreak>=25) {
            if (lastSpace!=0) {
                [resultChoice2 insertString:@"\n" atIndex: lastSpace];
            } else {
                // we have not found a space in 10 chars, so break where there is no space.
                // no H&J engine here, so we can add the - or not.
                [resultChoice2 insertString:@"-\n" atIndex: i];
                i++;  // since were adding a character, dont skip a character.
            }
            lastbreak=0;
            lastSpace=0;
        }
        i--;
    }
}
-(void)formatChoice3
{
    
    resultChoice3 = [NSMutableString stringWithString:choice3];
    int lastSpace=0;
    int lastbreak=0;
    NSUInteger i = [resultChoice3 length];
    while(i > 0) {
        if ([resultChoice3 characterAtIndex:i-1]==' ')
            lastSpace=i;
        lastbreak++;
        if (lastbreak>=25) {
            if (lastSpace!=0) {
                [resultChoice3 insertString:@"\n" atIndex: lastSpace];
            } else {
                // we have not found a space in 10 chars, so break where there is no space.
                // no H&J engine here, so we can add the - or not.
                [resultChoice3 insertString:@"-\n" atIndex: i];
                i++;  // since were adding a character, dont skip a character.
            }
            lastbreak=0;
            lastSpace=0;
        }
        i--;
    }
    
}


















+(id) scene
{
    
    CCScene *scene = [super scene];
    QuizLayer2* layer = [QuizLayer2 node];
	[scene addChild: layer];
	return scene;
}



@end
