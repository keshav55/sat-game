//
//  QuizLayer5.h
//  SatGame
//
//  Created by Keshav Rao on 8/24/13.
//
//

#import "StyledCCLayer.h"
#import "cocos2d.h"
#import "ResultPopup.h"
#import "WrongResultPopup.h"
#import "GameData.h"


@class CCControlButton, ResultPopup;
@interface QuizLayer5 : StyledCCLayer <ResultDelegate, WrongResultDelegate>

{
	//BOOL to keep track of whether the player is in the game (as opposed to in more games / how to play)
    BOOL inGame;
    
    CGSize size;
    
    CCControlButton *choiceA;
	CCControlButton *choiceB;
    CCControlButton *choiceC;
    CCControlButton *choiceD;
    int points;
    
    NSBundle *bundle;
    NSString *textFilePath;
    NSString *fileContents;
    NSString *otherdef;
    NSString *otherword;
    
    
    NSMutableArray *lines;
    NSMutableDictionary *dict;
    
    
    NSString *word;
    NSString *def;
    
    NSMutableString *resultDef;
    NSMutableString *resultChoice1;
    NSMutableString *resultChoice2;
    NSMutableString *resultChoice3;
    
    NSString *choice1;
    NSString *choice2;
    NSString *choice3;
    
    //Timer Stuff
    CCLabelTTF *timeLabel;
    ccTime totalTime;
    int myTime; //this is your incremented time.
    
    int currentTime;
    
    NSMutableArray *defchoices;
    
    
    GameData *data;
    
    
    
    int y1;
    int y2;
    int y3;
    int y4;
    
    CCLabelTTF *labelQuestion;
    
    
    
    int end;
    __weak ResultPopup* result;
    __weak WrongResultPopup* wResult;
}



-(void)createQuestion;
//-(void) randomPoint;

@end
