//
//  RecapLayer
//  SatGame
//
//  Created by Keshav Rao on 7/12/13.
//
//

#import "GameLayer.h"
#import "ChoiceLayer.h"
#import "CCControlButton.h"
#import "Points.h"
#import "GameMechanics.h"
#import "RecapLayer.h"
#import "CCDirector+PopTransition.h"


@implementation RecapLayer





-(id) init
{
	if ((self = [super init]))
	{
        [self addBackButton];
		
        
        NSArray *allwords = [MGWU objectForKey:@"otherwords"];
        
        
        if (allwords.count == 0)
        {
            QuizLayer *quizLayer = [[QuizLayer alloc] init];
            
            [quizLayer createQuestion];
            
            allwords = [MGWU objectForKey:@"otherwords"];
            
            
        }
        
        
        int v = [[MGWU objectForKey:@"q"]intValue];
        int w = [[MGWU objectForKey:@"q2"]intValue];
        int x = [[MGWU objectForKey:@"q3"]intValue];
        int y = [[MGWU objectForKey:@"q4"]intValue];
        int z = [[MGWU objectForKey:@"q5"]intValue];
        
        
        
        word = [allwords objectAtIndex:v];
        word2 = [allwords objectAtIndex:w];
        word3 = [allwords objectAtIndex:x];
        word4 = [allwords objectAtIndex:y];
        word5 = [allwords objectAtIndex:z];
        
        
        CCLabelTTF *first = [CCLabelTTF labelWithString:word fontName:@"Roboto-Light" fontSize:20];
        first.position = ccp(150, 380);
        [self addChild:first];
        
        CCLabelTTF *second = [CCLabelTTF labelWithString:word2 fontName:@"Roboto-Light" fontSize:20];
        second.position = ccp(150, 300);
        [self addChild:second];
        
        CCLabelTTF *third = [CCLabelTTF labelWithString:word3 fontName:@"Roboto-Light" fontSize:20];
        third.position = ccp(150, 220);
        [self addChild:third];
        
        CCLabelTTF *fourth = [CCLabelTTF labelWithString:word4 fontName:@"Roboto-Light" fontSize:20];
        fourth.position = ccp(150, 140);
        [self addChild:fourth];
        
        CCLabelTTF *fifth = [CCLabelTTF labelWithString:word5 fontName:@"Roboto-Light" fontSize:20];
        fifth.position = ccp(150, 60);
        [self addChild:fifth];
        
        [self addNavBarWithTitle:@"Review"];
        [self addBackButton];
        
        
        
        
        
        NSMutableArray *testanswers = [MGWU objectForKey:@"testanswers"];
        

        BOOL question1 = [[testanswers objectAtIndex:0] boolValue];
        BOOL question2 = [[testanswers objectAtIndex:1] boolValue];
        BOOL question3 = [[testanswers objectAtIndex:2] boolValue];
        BOOL question4 = [[testanswers objectAtIndex:3] boolValue];
        BOOL question5 = [[testanswers objectAtIndex:4] boolValue];
        
        
        
        int score = 0;
        CCSprite* check1 = [CCSprite spriteWithFile:@"check.png"];
        check1.position = ccp(80, 380);
        [self addChild:check1];
        
        CCSprite* check2 = [CCSprite spriteWithFile:@"check.png"];
        check2.position = ccp(80, 300);
        [self addChild:check2];
        
        CCSprite* check3 = [CCSprite spriteWithFile:@"check.png"];
        check3.position = ccp(80, 220);
        [self addChild:check3];
        
        CCSprite* check4 = [CCSprite spriteWithFile:@"check.png"];
        check4.position = ccp(80, 140);
        [self addChild:check4];
        
        CCSprite* check5 = [CCSprite spriteWithFile:@"check.png"];
        check5.position = ccp(80, 60);
        [self addChild:check5];
        
        CCSprite* wrong1 = [CCSprite spriteWithFile:@"X.png"];
        wrong1.position = ccp(80, 380);
        [self addChild:wrong1];
        
        CCSprite* wrong2 = [CCSprite spriteWithFile:@"X.png"];
        wrong2.position = ccp(80, 300);
        [self addChild:wrong2];
        
        CCSprite* wrong3 = [CCSprite spriteWithFile:@"X.png"];
        wrong3.position = ccp(80, 220);
        [self addChild:wrong3];
        
        CCSprite* wrong4 = [CCSprite spriteWithFile:@"X.png"];
        wrong4.position = ccp(80, 140);
        [self addChild:wrong4];
        
        CCSprite* wrong5 = [CCSprite spriteWithFile:@"X.png"];
        wrong5.position = ccp(80, 60);
        [self addChild:wrong5];
        
        //Opponent
        
        
        CCSprite* ocheck1 = [CCSprite spriteWithFile:@"check.png"];
        ocheck1.position = ccp(220, 380);
        [self addChild:ocheck1];
        
        CCSprite* ocheck2 = [CCSprite spriteWithFile:@"check.png"];
        ocheck2.position = ccp(220, 300);
        [self addChild:ocheck2];
        
        CCSprite* ocheck3 = [CCSprite spriteWithFile:@"check.png"];
        ocheck3.position = ccp(220, 220);
        [self addChild:ocheck3];
        
        CCSprite* ocheck4 = [CCSprite spriteWithFile:@"check.png"];
        ocheck4.position = ccp(220, 140);
        [self addChild:ocheck4];
        
        CCSprite* ocheck5 = [CCSprite spriteWithFile:@"check.png"];
        ocheck5.position = ccp(220, 60);
        [self addChild:ocheck5];
        
        CCSprite* owrong1 = [CCSprite spriteWithFile:@"X.png"];
        owrong1.position = ccp(220, 380);
        [self addChild:owrong1];
        
        CCSprite* owrong2 = [CCSprite spriteWithFile:@"X.png"];
        owrong2.position = ccp(220, 300);
        [self addChild:owrong2];
        
        CCSprite* owrong3 = [CCSprite spriteWithFile:@"X.png"];
        owrong3.position = ccp(220, 220);
        [self addChild:owrong3];
        
        CCSprite* owrong4 = [CCSprite spriteWithFile:@"X.png"];
        owrong4.position = ccp(220, 140);
        [self addChild:owrong4];
        
        CCSprite* owrong5 = [CCSprite spriteWithFile:@"X.png"];
        owrong5.position = ccp(220, 60);
        [self addChild:owrong5];
        
        if (question1 == YES)
        {
            NSLog(@"yes");
            score++;
            wrong1.visible = NO;
            
        }
        else {
            NSLog(@"no");
            check1.visible = NO;
        }
        if (question2 == YES)
        {
            NSLog(@"yes");
            score++;
            wrong2.visible = NO;
            
        }
        else {
            NSLog(@"no");
            check2.visible = NO;
        }
        if (question3 == YES)
        {
            NSLog(@"yes");
            score++;
            wrong3.visible = NO;
            
        }
        else {
            NSLog(@"no");
            check3.visible = NO;
        }
        if (question4 == YES)
        {
            NSLog(@"yes");
            score++;
            wrong4.visible = NO;
        }
        else {
            NSLog(@"no");
            check4.visible = NO;
        }
        if (question5 == YES)
        {
            NSLog(@"yes");
            score++;
            wrong5.visible = NO;
            
        }
        else {
            NSLog(@"no");
            check5.visible = NO;
        }
        
        NSMutableArray *opptestanswers = [MGWU objectForKey:@"opptestanswers"];
        
        if ([opptestanswers  isEqual:@"yes"]) {
            ocheck1.visible = NO;
            ocheck2.visible = NO;
            ocheck3.visible = NO;
            ocheck4.visible = NO;
            ocheck5.visible = NO;
            owrong1.visible = NO;
            owrong2.visible = NO;
            owrong3.visible = NO;
            owrong4.visible = NO;
            owrong5.visible = NO;
        }
        else {
            
            BOOL oppquestion1 = [[opptestanswers objectAtIndex:0] boolValue];
            BOOL oppquestion2 = [[opptestanswers objectAtIndex:1] boolValue];
            BOOL oppquestion3 = [[opptestanswers objectAtIndex:2] boolValue];
            BOOL oppquestion4 = [[opptestanswers objectAtIndex:3] boolValue];
            BOOL oppquestion5 = [[opptestanswers objectAtIndex:4] boolValue];
            
            
            if (oppquestion1 == YES)
            {
                NSLog(@"yes");
                owrong1.visible = NO;
                
            }
            else {
                NSLog(@"no");
                ocheck1.visible = NO;
            }
            if (oppquestion2 == YES)
            {
                NSLog(@"yes");
                owrong2.visible = NO;
                
            }
            else {
                NSLog(@"no");
                ocheck2.visible = NO;
            }
            if (oppquestion3 == YES)
            {
                NSLog(@"yes");
                owrong3.visible = NO;
                
            }
            else {
                NSLog(@"no");
                ocheck3.visible = NO;
            }
            if (oppquestion4 == YES)
            {
                NSLog(@"yes");
                owrong4.visible = NO;
            }
            else {
                NSLog(@"no");
                ocheck4.visible = NO;
            }
            if (oppquestion5 == YES)
            {
                NSLog(@"yes");
                owrong5.visible = NO;
                
            }
            else {
                NSLog(@"no");
                ocheck5.visible = NO;
            }
            
        }
        
        NSString *finalscore = [NSString stringWithFormat:@"%d" @"/5", score];
        CCLabelTTF *scorelabel = [CCLabelTTF labelWithString:finalscore fontName:@"Roboto-Light" fontSize:20];
        scorelabel.position = ccp(20,40);
        [self addChild:scorelabel];
        
        NSNumber *userscore = [NSNumber numberWithInt:score];
        [MGWU setObject:userscore forKey:@"userquizscore"];
        //
        //
        //    CCLabelTTF *points = [CCLabelTTF labelWithString:score fontName:@"Times New Roman" fontSize:20];
        //    points.position = ccp(100, 60);
        //    [self addChild:points];
        
        NSNumber *oppquizscore = [MGWU objectForKey:@"oppquizscore"];
        
        if (oppquizscore) {
            
            int p1 = [userscore intValue];
            int p2 = [oppquizscore intValue];
            
            if (p1 > p2) {
                NSLog(@"You won!");
            }
            if (p1 < p2) {
                NSLog(@"You lost!");
            }
            if (p1 == p2){
                NSLog(@"You tied!");
            }
            
        }
        
        
        
	}
    
	return self;
}

-(void) back
{
    //pop scene, slide in new scene from the left
    [CCDirector.sharedDirector popSceneWithTransition:[CCTransitionSlideInL class] duration:0.25f];
    
}


+(id) scene
{
    CCScene *scene = [super scene];
    RecapLayer* layer = [RecapLayer node];
	[scene addChild: layer];
	return scene;
}


@end
