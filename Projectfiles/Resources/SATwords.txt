abase   to humiliate, degrade
abate   to reduce, lessen
abdicate	to give up a position, usually one of leadership
abduct	to kidnap, take by force
aberration	something that differs from the norm
abet	to aid, help, encourage
abhor	to hate, detest
abide	to put up with; to remain
abject	wretched, pitiful
abjure	to reject, renounce
abnegation  denial of comfort to oneself
abort	to give up on a half finished project or effort
abridge	to cut down, shorten; shortened
abrogate	to abolish, usually by authority
abscond	to sneak away and hide
absolution	freedom from blame, guilt, sin
abstain	to freely choose not to commit an action
abstruse	hard to comprehend
accede	to agree
accentuate	to stress, highlight
accessible	obtainable, reachable
acclaim	high praise
accolade    high praise, special distinction
accommodating	helpful, obliging, polite
accord	an agreement
accost	to confront verbally
accretion	slow growth in size or amount
acerbic	biting, bitter in tone or taste
acquiesce	to agree without protesting
acrimony	bitterness, discord
acumen	keen insight
acute	sharp, severe; having keen insight
adamant	impervious, immovable, unyielding
adept	extremely skilled
adhere	to stick to something; to follow devoutly
admonish	to caution, criticize, reprove
adorn	to decorate
adroit	skillful, dexterous