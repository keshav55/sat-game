//
//  PopupLayer.h
//  Ghost
//
//  Created by Brian Chu on 1/15/13.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "StyledCCLayer.h"


@protocol ResultDelegate <NSObject>
-(void)dismiss;
@end

@interface ResultPopup : StyledCCLayer
{
	//Labels
	CCLabelTTF* titleLabel;
	CCLabelTTF* messageLabel;
    CCSprite *result;
	
	//Delegate to dismiss popup
	id<ResultDelegate> delegate;
}
@property CCLabelTTF *titleLabel, *messageLabel;
@property id<ResultDelegate> delegate;

-(void) rescaleTitleWithString: (NSString*) string;
-(void) rescaleMessageWithString: (NSString*) string;

@end
