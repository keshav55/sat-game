//
//  STYLES.g.h
//  _MGWU-Level-Template_
//
//  Created by Benjamin Encz on 5/14/13.
//  Modified by Dion Larson.
//

#ifndef _MGWU_Level_Template__STYLES_h
#define _MGWU_Level_Template__STYLES_h

#define SCREEN_BG_COLOR ccc4(255, 255, 255, 255)
#define SCREEN_BG_COLOR_TRANSPARENT ccc4(0, 0, 0, 200)

#define DEFAULT_FONT_COLOR ccc3(0, 0, 0)
#define WHITE_FONT_COLOR ccc3(255, 255, 255)
#define DEFAULT_FONT @"Roboto-Light"

#define INVERSE_FONT_COLOR ccc3(255, 255, 255)


#endif
