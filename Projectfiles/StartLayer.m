//
//  StartLayer.m
//  Ghost
//
//  Created by Brian Chu on 10/29/12.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "StartLayer.h"
#import "InterfaceLayer.h"
#import "CCControlButton.h"
#import "TutorialLayer.h"
#import "TableLayer.h"
#import "QuizLayer.h"


@interface StartLayer ()
@end

@implementation StartLayer

-(id) init
{
	if ((self = [super init]))
	{
        
        
        
		inGame=NO;
        CGSize screenSize = CCDirector.sharedDirector.winSize;
        
        //Ghost logo
        CCLabelTTF *SAT = [CCLabelTTF labelWithString:@"SAT WARS" fontName:@"Roboto-Light" fontSize:70];
        SAT.color = ccc3(120, 0, 0);
        SAT.anchorPoint = ccp(0.5, 1.0);
        SAT.position = ccp(160, screenSize.height-23);
        
        
        CCSprite* background = [CCSprite spriteWithFile:@"logobackground-hd.png"];
        
        if ([[CCDirector sharedDirector] winSizeInPixels].height == 1024) {
            background = [CCSprite spriteWithFile:@"logobackground-ip5.png"];
            
        } else if ([[CCDirector sharedDirector] winSizeInPixels].height == 1136) {
            background = [CCSprite spriteWithFile:@"logobackground-ip5.png"];
            
        } else {
            background = [CCSprite spriteWithFile:@"logobackground-hd.png"];
            
        }

        
        
        
        
        CGSize imageSize = background.contentSize;
//       background.scaleX = screenSize.width;
//       background.scaleY = screenSize.height;
        
//        background.anchorPoint = ccp(0.5, 1.0);
        background.anchorPoint = ccp(0,0);
        background.position = ccp(0,0);
        [self addChild:background];
        //Play
//        playButton = [self standardButtonWithTitle:@"PLAY" fontSize:40 selector:@selector(play) preferredSize:CGSizeMake(156, 90)];
//        playButton.anchorPoint = ccp(0.5, 1.0);
//        playButton.position = ccp(SAT.position.x, SAT.position.y - SAT.contentSize.height - 8);
//      [self addChild:playButton];
//        playButton.visible = YES;
        
        CCMenuItemImage *settings = [CCMenuItemImage itemWithNormalImage:@"start_btn.png" selectedImage:@"start_btn.png" target:self selector:@selector(play)];
         CCMenuItemImage *settings1 = [CCMenuItemImage itemWithNormalImage:@"help_btn.png" selectedImage:@"help_btn.png" target:self selector:@selector(howToPlay)];
        
         CCMenuItemImage *settings2 = [CCMenuItemImage itemWithNormalImage:@"more_btn.png" selectedImage:@"more_btn.png" target:self selector:@selector(moreGames)];
    
        CCMenu *settingsMenu = [CCMenu menuWithItems:settings, nil];
        settingsMenu.position = ccp(SAT.position.x, SAT.position.y - SAT.contentSize.height - 278);
        [settingsMenu alignItemsVerticallyWithPadding:10.0f];
        [self addChild:settingsMenu];
        
        CCMenu *settingsMenu2 = [CCMenu menuWithItems: settings1,settings2, nil];
        settingsMenu2.position = ccp(settingsMenu.position.x, settingsMenu.position.y - 75);
        [settingsMenu2 alignItemsHorizontallyWithPadding:10.0f];
        [self addChild:settingsMenu2];
        //How to Play
//        CCControlButton* howToPlayButton = [self standardButtonWithTitle:@"HOW TO PLAY" fontSize:35 selector:@selector(howToPlay) preferredSize:CGSizeMake(307, 62)];
//        howToPlayButton.anchorPoint = playButton.anchorPoint;
//        howToPlayButton.position =  ccp(playButton.position.x, playButton.position.y - playButton.contentSize.height - 18);
//        [self addChild:howToPlayButton];

        //MoreGames
//        moreGamesButton = [self standardButtonWithTitle:@"MORE GAMES" fontSize:35 selector:@selector(moreGames) preferredSize:CGSizeMake(307, 62)];
//        moreGamesButton.anchorPoint =  howToPlayButton.anchorPoint;
//        moreGamesButton.position = ccp(howToPlayButton.position.x, howToPlayButton.position.y - howToPlayButton.contentSize.height - 12);
//        [self addChild:moreGamesButton];

        
        //If player has not completed the tutorial yet, hide the other buttons, otherwise show them
//        BOOL completedTutorial = [[NSUserDefaults standardUserDefaults] boolForKey:@"completedTutorial"];
//        if (!completedTutorial)
//        {
//            playButton.visible=NO;
//            moreGamesButton.visible=NO;
//        }
//        else
//        {
//            playButton.visible=YES;
//            moreGamesButton.visible=YES;
//        }
        
	}
	return self;
}

-(void)play
{
    //slide in scene from the right
    
    
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[InterfaceLayer scene]];
    [CCDirector.sharedDirector replaceScene:transition];
}

-(void)howToPlay
{

    //slide in scene from the right
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[TutorialLayer scene]];
    [CCDirector.sharedDirector replaceScene:transition];
}

- (void)moreGames
{
	[MGWU displayCrossPromo];
}

+(id) scene
{
    CCScene *scene = [super scene];
    StartLayer* layer = [StartLayer node];
	[scene addChild: layer];
	return scene;
}


@end
