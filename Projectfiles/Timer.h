//
//  Timer.h
//  ACT
//
//  Created by Keshav Rao on 6/26/13.
//
//

#import "StyledCCLayer.h"
#import "cocos2d.h"
#import "ResultPopup.h"

@interface Timer : StyledCCLayer <ResultDelegate>
{
    
    CCLabelTTF *timeLabel;
    ccTime totalTime;
    int myTime; //this is your incremented time.
    
    int currentTime;
    __weak ResultPopup* result;
    
    
}




@end
