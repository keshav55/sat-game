//
//  TutorialLayer.h
//  Ghost
//
//  Created by Ashutosh Desai on 1/14/12.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "StyledCCLayer.h"

@interface TutorialLayer : StyledCCLayer
{
    
	// Holds the current height and width of the screen
	int scrollHeight;
	int scrollWidth;
	int startHeight;
	int startWidth;
	int currentScreen;
	int totalScreens;
	int startPos;
}

-(id) initWithLayers:(NSMutableArray *)layers;;
-(void) setUpScore;

@end
