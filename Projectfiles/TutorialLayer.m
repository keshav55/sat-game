//
//  TutorialLayer.m
//  Ghost
//
//  Created by Ashutosh Desai on 1/14/12.
//  Copyright (c) 2012 makegameswithus inc. Free to use for all purposes.
//

#import "TutorialLayer.h"
#import "StartLayer.h"
#import "CCControlExtension.h"
#import "CCScrollLayer.h"
#import "InterfaceLayer.h"

@class CCControlButton;

@implementation TutorialLayer

-(id) init
{
    self = [super init];
    
   CGSize screenSize = [CCDirector sharedDirector].winSize;
    // create a blank layer for page 1
    CCLayer *pageOne = [[CCLayer alloc] init];
    
    // create a label for page 1
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"Page 1" fontName:@"Arial Rounded MT Bold" fontSize:44];
    CCSprite *pic1 = [CCSprite spriteWithFile:@"FirstTutorial.png"];
    pic1.position =  ccp(screenSize.width /2 - 10 , screenSize.height/2 );
    
    // add label to page 1 layer
    [pageOne addChild:pic1];
    
	//Log event that the tutorial was started (step 0)
    // create a blank layer for page 2
    CCLayer *pageTwo = [[CCLayer alloc] init];
    
    // create a custom font menu for page 2
    CCSprite *pic2 = [CCSprite spriteWithFile:@"SecondTutorial.png"];
    pic2.position =  ccp(screenSize.width /2 - 10 , screenSize.height/2 );
    
    // add label to page 1 layer
    [pageTwo addChild:pic2];
    

    // add menu to page 2
    ////////////////////////////////////////////////
    
    CCLayer *pageThree = [[CCLayer alloc] init];
    
    // create a custom font menu for page 2
    CCLabelTTF *label3 = [CCLabelTTF labelWithString:@"READY?" fontName:@"Roboto-Light" fontSize:44];
    label3.position =  ccp(screenSize.width /2 , screenSize.height/2 );
    
    CCControlButton* play = [self standardButtonWithTitle:@"PLAY" fontSize:35 selector:@selector(home) preferredSize:CGSizeMake(200, 62)];
    play.position = ccp(screenSize.width /2, screenSize.height /4);
    
    
    // add menu to page 2
    [pageThree addChild:label3];
    [pageThree addChild:play];
    
    
    pageOne.contentSize = screenSize;
    pageTwo.contentSize = screenSize;
    pageThree.contentSize = screenSize;
    
    // now create the scroller and pass-in the pages (set widthOffset to 0 for fullscreen pages)
    CCScrollLayer *scroller = [[CCScrollLayer alloc] initWithLayers:[NSMutableArray arrayWithObjects: pageOne,pageTwo,pageThree, nil] widthOffset: 0];
    
    // finally add the scroller to your scene
    [self addChild:scroller];
    
    return self;
	
}


-(void) home
{
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[InterfaceLayer scene]];
    [CCDirector.sharedDirector replaceScene:transition];

}



+(id) scene
{
    CCScene *scene = [super scene];
    TutorialLayer* layer = [TutorialLayer node];
	[scene addChild: layer];
	return scene;
}



@end
