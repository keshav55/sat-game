//
//  ExpLayer.m
//  SatGame
//
//  Created by Keshav Rao on 6/27/13.
//
//

#import "WinLayer.h"
#import "StyledCCLayer.h"
#import "QuizLayer.h"
#import "CCControlButton.h"
#import "GameLayer.h"
#import "InterfaceLayer.h"
#import "DefinitionViewController.h"



@interface WinLayer()
@end


@implementation WinLayer

-(id) init
{
    self = [super init];

	
    NSArray *allwords = [MGWU objectForKey:@"otherwords"];
    
    
    if (allwords.count == 0)
    {
        QuizLayer *quizLayer = [[QuizLayer alloc] init];
        
        [quizLayer createQuestion];
        
        allwords = [MGWU objectForKey:@"otherwords"];
        
        
    }
    
    
    int v = [[MGWU objectForKey:@"q"]intValue];
    int w = [[MGWU objectForKey:@"q2"]intValue];
    int x = [[MGWU objectForKey:@"q3"]intValue];
    int y = [[MGWU objectForKey:@"q4"]intValue];
    int z = [[MGWU objectForKey:@"q5"]intValue];
    
    
    
    word = [allwords objectAtIndex:v];
    word2 = [allwords objectAtIndex:w];
    word3 = [allwords objectAtIndex:x];
    word4 = [allwords objectAtIndex:y];
    word5 = [allwords objectAtIndex:z];
        

    CCLabelTTF *first = [CCLabelTTF labelWithString:word fontName:@"Roboto-Light" fontSize:20];
    first.position = ccp(150, 460);
    [self addChild:first];
    
    CCLabelTTF *second = [CCLabelTTF labelWithString:word2 fontName:@"Roboto-Light" fontSize:20];
    second.position = ccp(150, 380);
    [self addChild:second];
    
    CCLabelTTF *third = [CCLabelTTF labelWithString:word3 fontName:@"Roboto-Light" fontSize:20];
    third.position = ccp(150, 300);
    [self addChild:third];
    
    CCLabelTTF *fourth = [CCLabelTTF labelWithString:word4 fontName:@"Roboto-Light" fontSize:20];
    fourth.position = ccp(150, 220);
    [self addChild:fourth];
    
    CCLabelTTF *fifth = [CCLabelTTF labelWithString:word5 fontName:@"Roboto-Light" fontSize:20];
    fifth.position = ccp(150, 140);
    [self addChild:fifth];
        
        
    CCControlButton *define1 = [self standardButtonWithTitle:@"DEFINE" font:@"Roboto-Light" fontSize:20 target:self selector:@selector(define1) preferredSize:CGSizeMake(200, 40)];
     CCControlButton *define2 = [self standardButtonWithTitle:@"DEFINE" font:@"Roboto-Light" fontSize:20 target:self selector:@selector(define2) preferredSize:CGSizeMake(200,40)];
     CCControlButton *define3 = [self standardButtonWithTitle:@"DEFINE" font:@"Roboto-Light" fontSize:20 target:self selector:@selector(define3) preferredSize:CGSizeMake(200, 40)];
     CCControlButton *define4 = [self standardButtonWithTitle:@"DEFINE" font:@"Roboto-Light" fontSize:20 target:self selector:@selector(define4) preferredSize:CGSizeMake(200, 40)];
     CCControlButton *define5 = [self standardButtonWithTitle:@"DEFINE" font:@"Roboto-Light" fontSize:20 target:self selector:@selector(define5) preferredSize:CGSizeMake(200, 40)];
    
    
    define1.position = ccp(first.position.x,first.position.y - 30);
    define2.position = ccp(second.position.x, second.position.y - 30);
    define3.position = ccp(third.position.x, third.position.y - 30);
    define4.position = ccp(fourth.position.x, fourth.position.y - 30);
    define5.position = ccp(fifth.position.x, fifth.position.y - 30);
    
    [self addChild:define1];
    [self addChild:define2];
    [self addChild:define3];
    [self addChild:define4];
    [self addChild:define5];
    
    
    
    NSMutableArray *testanswers = [MGWU objectForKey:@"testanswers"];
    
    BOOL question1 = [[testanswers objectAtIndex:0] boolValue];
    BOOL question2 = [[testanswers objectAtIndex:1] boolValue];
    BOOL question3 = [[testanswers objectAtIndex:2] boolValue];
    BOOL question4 = [[testanswers objectAtIndex:3] boolValue];
    BOOL question5 = [[testanswers objectAtIndex:4] boolValue];
    
    
  
    
    int score = 0;
    CCSprite* check1 = [CCSprite spriteWithFile:@"check.png"];
    check1.position = ccp(80, first.position.y);
    [self addChild:check1];
    
    CCSprite* check2 = [CCSprite spriteWithFile:@"check.png"];
    check2.position = ccp(80, second.position.y);
    [self addChild:check2];
    
    CCSprite* check3 = [CCSprite spriteWithFile:@"check.png"];
    check3.position = ccp(80, third.position.y);
    [self addChild:check3];

    CCSprite* check4 = [CCSprite spriteWithFile:@"check.png"];
    check4.position = ccp(80, fourth.position.y);
    [self addChild:check4];
    
    CCSprite* check5 = [CCSprite spriteWithFile:@"check.png"];
    check5.position = ccp(80, fifth.position.y);
    [self addChild:check5];
    
    CCSprite* wrong1 = [CCSprite spriteWithFile:@"X.png"];
    wrong1.position = ccp(80, first.position.y);
    [self addChild:wrong1];
    
    CCSprite* wrong2 = [CCSprite spriteWithFile:@"X.png"];
    wrong2.position = ccp(80, second.position.y);
    [self addChild:wrong2];
    
    CCSprite* wrong3 = [CCSprite spriteWithFile:@"X.png"];
    wrong3.position = ccp(80, third.position.y);
    [self addChild:wrong3];
    
    CCSprite* wrong4 = [CCSprite spriteWithFile:@"X.png"];
    wrong4.position = ccp(80, fourth.position.y);
    [self addChild:wrong4];
    
    CCSprite* wrong5 = [CCSprite spriteWithFile:@"X.png"];
    wrong5.position = ccp(80, fifth.position.y);
    [self addChild:wrong5];
    
    //Opponent

    
    CCSprite* ocheck1 = [CCSprite spriteWithFile:@"check.png"];
    ocheck1.position = ccp(220, first.position.y);
    [self addChild:ocheck1];
    
    CCSprite* ocheck2 = [CCSprite spriteWithFile:@"check.png"];
    ocheck2.position = ccp(220, second.position.y);
    [self addChild:ocheck2];
    
    CCSprite* ocheck3 = [CCSprite spriteWithFile:@"check.png"];
    ocheck3.position = ccp(220, third.position.y);
    [self addChild:ocheck3];
    
    CCSprite* ocheck4 = [CCSprite spriteWithFile:@"check.png"];
    ocheck4.position = ccp(220, fourth.position.y);
    [self addChild:ocheck4];
    
    CCSprite* ocheck5 = [CCSprite spriteWithFile:@"check.png"];
    ocheck5.position = ccp(220, fifth.position.y);
    [self addChild:ocheck5];
    
    CCSprite* owrong1 = [CCSprite spriteWithFile:@"X.png"];
    owrong1.position = ccp(220, first.position.y);
    [self addChild:owrong1];
    
    CCSprite* owrong2 = [CCSprite spriteWithFile:@"X.png"];
    owrong2.position = ccp(220,second.position.y);
    [self addChild:owrong2];
    
    CCSprite* owrong3 = [CCSprite spriteWithFile:@"X.png"];
    owrong3.position = ccp(220, third.position.y);
    [self addChild:owrong3];
    
    CCSprite* owrong4 = [CCSprite spriteWithFile:@"X.png"];
    owrong4.position = ccp(220, fourth.position.y);
    [self addChild:owrong4];
    
    CCSprite* owrong5 = [CCSprite spriteWithFile:@"X.png"];
    owrong5.position = ccp(220, fifth.position.y);
    [self addChild:owrong5];
    
    if (question1 == YES)
    {
        NSLog(@"yes");
        score++;
        wrong1.visible = NO;
        define1.visible = NO;
        
    }
    else {
        NSLog(@"no");
        check1.visible = NO;

    }
    if (question2 == YES)
    {
        NSLog(@"yes");
        score++;
        wrong2.visible = NO;
        define2.visible = NO;
        
    }
    else {
        NSLog(@"no");
        check2.visible = NO;
    }
    if (question3 == YES)
    {
        NSLog(@"yes");
        score++;
        wrong3.visible = NO;
        define3.visible = NO;
        
    }
    else {
        NSLog(@"no");
        check3.visible = NO;
    }
    if (question4 == YES)
    {
        NSLog(@"yes");
        score++;
        wrong4.visible = NO;
          define4.visible = NO;
    }
    else {
        NSLog(@"no");
        check4.visible = NO;
        
    }
    if (question5 == YES)
    {
        NSLog(@"yes");
        score++;
        wrong5.visible = NO;
         define5.visible = NO;
        
    }
    else {
        NSLog(@"no");
        check5.visible = NO;

    }
    NSMutableArray *opptestanswers = [MGWU objectForKey:@"opptestanswers"];

    if ([opptestanswers  isEqual:@"yes"]) {
            ocheck1.visible = NO;
            ocheck2.visible = NO;
            ocheck3.visible = NO;
            ocheck4.visible = NO;
            ocheck5.visible = NO;
            owrong1.visible = NO;
            owrong2.visible = NO;
            owrong3.visible = NO;
            owrong4.visible = NO;
            owrong5.visible = NO;
        }
    else {
        
    BOOL oppquestion1 = [[opptestanswers objectAtIndex:0] boolValue];
    BOOL oppquestion2 = [[opptestanswers objectAtIndex:1] boolValue];
    BOOL oppquestion3 = [[opptestanswers objectAtIndex:2] boolValue];
    BOOL oppquestion4 = [[opptestanswers objectAtIndex:3] boolValue];
    BOOL oppquestion5 = [[opptestanswers objectAtIndex:4] boolValue];
    
    
        if (oppquestion1 == YES)
        {
            NSLog(@"yes");
            owrong1.visible = NO;
            
        }
        else {
            NSLog(@"no");
            ocheck1.visible = NO;
        }
        if (oppquestion2 == YES)
        {
            NSLog(@"yes");
            owrong2.visible = NO;
            
        }
        else {
            NSLog(@"no");
            ocheck2.visible = NO;
        }
        if (oppquestion3 == YES)
        {
            NSLog(@"yes");
            owrong3.visible = NO;
            
        }
        else {
            NSLog(@"no");
            ocheck3.visible = NO;
        }
        if (oppquestion4 == YES)
        {
            NSLog(@"yes");
            owrong4.visible = NO;
        }
        else {
            NSLog(@"no");
            ocheck4.visible = NO;
        }
        if (oppquestion5 == YES)
        {
            NSLog(@"yes");
            owrong5.visible = NO;
            
        }
        else {
            NSLog(@"no");
            ocheck5.visible = NO;
        }

    }
    
    NSString *finalscore = [NSString stringWithFormat:@"%d" @"/5", score];
    CCLabelTTF *scorelabel = [CCLabelTTF labelWithString:finalscore fontName:@"Roboto-Light" fontSize:20];
    scorelabel.position = ccp(20,40);
    [self addChild:scorelabel];
    
    NSNumber *userquizscore = [NSNumber numberWithInt:score];
    [MGWU setObject:userquizscore forKey:@"userquizscore"];
//
//
//    CCLabelTTF *points = [CCLabelTTF labelWithString:score fontName:@"Times New Roman" fontSize:20];
//    points.position = ccp(100, 60);
//    [self addChild:points];
    
    NSNumber *oppquizscore = [MGWU objectForKey:@"oppquizscore"];
    if (!([oppquizscore intValue] == 10)) {
        
        int p1 = [userquizscore intValue];
        int p2 = [oppquizscore intValue];
        
        [MGWU setObject:oppquizscore forKey:@"oldoquizscore"];
        if (p1 > p2) {
            NSLog(@"You won!");
            int uscore = [[MGWU objectForKey:@"userscore"]intValue];
            uscore++;
            NSNumber *userscore = [NSNumber numberWithInt:uscore];
            [MGWU setObject:userscore forKey:@"userscore"];
            NSDictionary *user = [MGWU objectForKey:@"user"];
            NSString *victory = [NSString stringWithFormat:@" %@ won the last game %d - %d",[user objectForKey:@"username"], p1 , p2];
            [MGWU setObject:victory forKey:@"victory"];
            NSLog(@"%@", victory);
        }
        if (p1 < p2) {
            NSLog(@"You lost!");
            int uscore = [[MGWU objectForKey:@"oppscore"]intValue];
            uscore++;
            NSNumber *oppscore = [NSNumber numberWithInt:uscore];
            [MGWU setObject:oppscore forKey:@"oppscore"];
            NSString *victory = [NSString stringWithFormat:@" %@ won the last game %d - %d",[MGWU objectForKey:@"opponent"], p2 , p1];
            NSLog(@"%@", victory);
            [MGWU setObject:victory forKey:@"victory"];


        }
        if (p1 == p2){
            NSLog(@"You tied!");
            
        
        }
         
    }
        
        
       
        
        
     
    CCControlButton *play = [self standardButtonWithTitle:@"HOME" font:@"Roboto-Light" fontSize:16 target:self selector:@selector(play) preferredSize:CGSizeMake(200,60)];
     play.position = ccp(265 , 20);
        [self addChild:play];


 

    
        
        
        
        
        
	return self;
}
-(void)play
{
    //slide in scene from the right
    NSDictionary *move;

    move = [MGWU objectForKey:@"move"];
    
    if (move == nil) {
       move = [[NSUserDefaults standardUserDefaults]objectForKey:@"move"];
    }
    
    if (move != nil) {
    
    [MGWU setObject:move forKey:@"move"];
        
    }
    
    GameLayer *gameLayer = [[GameLayer alloc] init];
    
    [gameLayer sendquiz];
    
    
    

   
}
-(void)winPlay
{
    //slide in scene from the right
    CCTransitionSlideInR* transition = [CCTransitionSlideInR transitionWithDuration:0.25f scene:[InterfaceLayer scene]];
    [CCDirector.sharedDirector replaceScene:transition];
}

-(void) define1
{
    DefinitionViewController *dvc = [[DefinitionViewController alloc] init];
    
    //Ghost uses interface builder to set up the text view. Here, we have to manually set up the text view
    dvc.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 260, 320, 220)];
    dvc.textView.backgroundColor=[UIColor colorWithWhite:0.21 alpha:1];
    dvc.textView.textColor = [UIColor colorWithWhite:1 alpha:1];
    
    dvc.definition = [MGWU objectForKey:@"def1"];
    [dvc setModalTransitionStyle:UIModalTransitionStylePartialCurl]; //set transition type - page curl
    dvc.view.backgroundColor = [UIColor colorWithWhite:0.21 alpha:1];
    
    [dvc.view addSubview:dvc.textView];
    
    //add view controller to CCDirector
    [CCDirector.sharedDirector presentViewController:dvc animated:YES completion:nil];
}
-(void) define2
{
    DefinitionViewController *dvc = [[DefinitionViewController alloc] init];
    
    //Ghost uses interface builder to set up the text view. Here, we have to manually set up the text view
    dvc.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 260, 320, 220)];
    dvc.textView.backgroundColor=[UIColor colorWithWhite:0.21 alpha:1];
    dvc.textView.textColor = [UIColor colorWithWhite:1 alpha:1];
    
    dvc.definition = [MGWU objectForKey:@"def2"];
    [dvc setModalTransitionStyle:UIModalTransitionStylePartialCurl]; //set transition type - page curl
    dvc.view.backgroundColor = [UIColor colorWithWhite:0.21 alpha:1];
    
    [dvc.view addSubview:dvc.textView];
    
    //add view controller to CCDirector
    [CCDirector.sharedDirector presentViewController:dvc animated:YES completion:nil];
}
-(void) define3
{
    DefinitionViewController *dvc = [[DefinitionViewController alloc] init];
    
    //Ghost uses interface builder to set up the text view. Here, we have to manually set up the text view
    dvc.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 260, 320, 220)];
    dvc.textView.backgroundColor=[UIColor colorWithWhite:0.21 alpha:1];
    dvc.textView.textColor = [UIColor colorWithWhite:1 alpha:1];
    
    dvc.definition = [MGWU objectForKey:@"def3"];
    [dvc setModalTransitionStyle:UIModalTransitionStylePartialCurl]; //set transition type - page curl
    dvc.view.backgroundColor = [UIColor colorWithWhite:0.21 alpha:1];
    
    [dvc.view addSubview:dvc.textView];
    
    //add view controller to CCDirector
    [CCDirector.sharedDirector presentViewController:dvc animated:YES completion:nil];
}
-(void) define4
{
    DefinitionViewController *dvc = [[DefinitionViewController alloc] init];
    
    //Ghost uses interface builder to set up the text view. Here, we have to manually set up the text view
    dvc.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 260, 320, 220)];
    dvc.textView.backgroundColor=[UIColor colorWithWhite:0.21 alpha:1];
    dvc.textView.textColor = [UIColor colorWithWhite:1 alpha:1];
    
    dvc.definition = [MGWU objectForKey:@"def4"];
    [dvc setModalTransitionStyle:UIModalTransitionStylePartialCurl]; //set transition type - page curl
    dvc.view.backgroundColor = [UIColor colorWithWhite:0.21 alpha:1];
    
    [dvc.view addSubview:dvc.textView];
    
    //add view controller to CCDirector
    [CCDirector.sharedDirector presentViewController:dvc animated:YES completion:nil];
}
-(void) define5
{
    DefinitionViewController *dvc = [[DefinitionViewController alloc] init];
    
    //Ghost uses interface builder to set up the text view. Here, we have to manually set up the text view
    dvc.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 260, 320, 220)];
    dvc.textView.backgroundColor=[UIColor colorWithWhite:0.21 alpha:1];
    dvc.textView.textColor = [UIColor colorWithWhite:1 alpha:1];
    
    dvc.definition = [MGWU objectForKey:@"def5"];
    [dvc setModalTransitionStyle:UIModalTransitionStylePartialCurl]; //set transition type - page curl
    dvc.view.backgroundColor = [UIColor colorWithWhite:0.21 alpha:1];
    
    [dvc.view addSubview:dvc.textView];
    
    //add view controller to CCDirector
    [CCDirector.sharedDirector presentViewController:dvc animated:YES completion:nil];
}



+(id) scene
{
    CCScene *scene = [super scene];
    WinLayer* layer = [WinLayer node];
	[scene addChild: layer];
	return scene;
}
@end