//
//  WrongResultLayer.h
//  SatGame
//
//  Created by Keshav Rao on 7/11/13.
//
//

#import "StyledCCLayer.h"


@protocol WrongResultDelegate <NSObject>
-(void)leave;
@end

@interface WrongResultPopup : StyledCCLayer
{
	//Labels
	CCLabelTTF* titleLabel;
	CCLabelTTF* messageLabel;
    CCSprite *result;
	
	//Delegate to dismiss popup
	id<WrongResultDelegate> delegate;
}
@property CCLabelTTF *titleLabel, *messageLabel;
@property id<WrongResultDelegate> delegate;

-(void) rescaleTitleWithString: (NSString*) string;
-(void) rescaleMessageWithString: (NSString*) string;

@end

